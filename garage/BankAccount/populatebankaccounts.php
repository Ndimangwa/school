<?php 
require_once("../../class/system.php");
$config="../../config.php";
include($config);
$countSuccessful = 0;
$countFailed = 0;
class LineObject	{
	private $formFourIndexNumber;
	private $formSixIndexNumber;
	private $accountName;
	private $sexName;
	private $registrationNumber;
	private $courseName;
	private $yearOfStudy;
	private $bankName;
	private $accountNumber;
	private $line;
	public function getFormFourIndexNumber()	{ return $this->formFourIndexNumber; }
	public function getFormSixIndexNumber()	{ return $this->formSixIndexNumber; }
	public function getAccountName()	{ return $this->accountName; }
	public function getSexName()	{ return $this->sexName; }
	public function getRegistrationNumber()	{ return $this->registrationNumber; }
	public function getCourseName()	{ return $this->courseName; }
	public function getYearOfStudy()	{ return $this->yearOfStudy; }
	public function getBankName()	{ return $this->bankName; }
	public function getAccountNumber()	{ return $this->accountNumber; }
	public function getLine()	{ return $this->line; }
	public function __construct($line)	{
		$this->line = $line;
		$lineArr = explode(",", $line);
		if (sizeof($lineArr) < 10) Object::shootException("Data Width is not valid");
		$this->formFourIndexNumber = str_replace("!comma!", ",", $lineArr[1]);
		$this->formSixIndexNumber = str_replace("!comma!", ",", $lineArr[2]);
		$this->accountName = str_replace("!comma!", ",", $lineArr[3]);
		$this->sexName = str_replace("!comma!", ",", $lineArr[4]);
		$this->registrationNumber = str_replace("!comma!", ",", $lineArr[5]);
		$this->courseName = str_replace("!comma!", ",", $lineArr[6]);
		$this->yearOfStudy = str_replace("!comma!", ",", $lineArr[7]);
		$this->bankName = str_replace("!comma!", ",", $lineArr[8]);
		$this->accountNumber = str_replace("!comma!", ",", $lineArr[9]);
	}
}
//Command Syntanx php populatebankaccounts.php sourcefile.csv errorfile.csv 
//The errorfile.csv can be used as a sourcefile in the next round
if (! (isset($argv[1]) && isset($argv[2]))) die("\nCommand Syntanx: php populatebankaccounts.php sourcefile.csv errorfile.csv\n");
$sourcefile=$argv[1];
$errorfile=$argv[2];
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
if (! file_exists($sourcefile)) Object::shootException("\nThe source file is not available\n");
$sourceFile1 = fopen($sourcefile, "r") or Object::shootException("\nCould not open source file for reading\n");
$errorFile1 = fopen($errorfile, "w") or Object::shootException("\nCould not open an error file for writing\n");
$linecount = 0;
while (($line = fgets($sourceFile1)) !== false)	{
	$linecount++;
	$line = str_replace("\n","", str_replace("\r", "", $line));
	if ($linecount == 1)	{
		//Write Headers 
		$line .= "\n";
		fwrite($errorFile1, $line) or Object::shootException("Could not write to Line $linecount"); //We must initialize header
		continue;
	}
	$lineObject1 = null;
	$student1 = null;
	try {
		$lineObject1 = new LineObject($line);
		$registrationNumber = $lineObject1->getRegistrationNumber();
		$query = "SELECT studentId FROM students WHERE registrationNumber='$registrationNumber'";
		$result=mysql_db_query($database, $query, $conn) or Object::shootException("Could not Execute Query");
		list($studentId)=mysql_fetch_row($result);
		$student1 = new Student($database, $studentId, $conn);
		//Load xml data 
		$student1->loadXMLFolder("../../data/students");
	} catch (Exception $e)	{
		$countFailed++;
		try {
			$line .= "\n";
			fwrite($errorFile1, $line) or Object::shootException("Could not write to Line $linecount"); //You must write data Info
		} catch (Exception $e)	{
			//Will default Continue
			echo ".";
		}
		echo ".";
		continue;
	}
	$doc = $student1->getDOMDocument();
	//Let us update FormFourIndex , levelId is 4 #levelId=4
	//FormSix levelId is, 8 ACSEE #levelId=8
	//FormFour
	$listOfAllNodes = FileFactory::findCustomNodes($doc, "0:accademicHistory;*:institution;0:levelId");
	if (! is_null($listOfAllNodes))	{
		//Form Four
		$nodeList = FileFactory::getListOfNodesWithValueFromCollection($listOfAllNodes, 0, "4", true);
		if (! is_null($nodeList))	{
			$institution = $nodeList[0]->parentNode;
			$indexNumber1 = $institution->getElementsByTagName("indexNumber");
			$indexNumber1 = $indexNumber1->item(0);
			if ($indexNumber1->hasChildNodes())	{
				//Clear All Childres 
				foreach ($indexNumber1->childNodes as $child1)	{
					$indexNumber1->removeChild($child1);
				}
			}
			$indexNumber1->appendChild($doc->createTextNode($lineObject1->getFormFourIndexNumber()));
		}
		//Form Six 
		$nodeList = FileFactory::getListOfNodesWithValueFromCollection($listOfAllNodes, 0, "8", true);
		if (! is_null($nodeList))	{
			$institution = $nodeList[0]->parentNode;
			$indexNumber1 = $institution->getElementsByTagName("indexNumber");
			$indexNumber1 = $indexNumber1->item(0);
			if ($indexNumber1->hasChildNodes())	{
				//Clear All Childres 
				foreach ($indexNumber1->childNodes as $child1)	{
					$indexNumber1->removeChild($child1);
				}
			}
			$indexNumber1->appendChild($doc->createTextNode($lineObject1->getFormSixIndexNumber()));
		}
	}
	//Now Proceed with bank Accounts
	$bankAccount1 = array();
	$bankAccount1['accountName'] = $lineObject1->getAccountName();
	$bankAccount1['bankAccount'] = $lineObject1->getAccountNumber();
	$bankAccount1['bankName'] = $lineObject1->getBankName();
	$bankAccount1['branchName'] = "";
	$promise1 = $student1->backupXMLFile();
	$promise1 = FileFactory::updateExistingFile($student1->getAbsoluteFilePath(), $doc);
	$promise1 = $student1->clearBankAccounts();
	$promise1 = $student1->addBankAccount($bankAccount1);
	if ($promise1->isPromising()) {
		echo "!";
		$countSuccessful++;
	} else {
		echo "P";
		$countFailed++;
	}
	
	//findParentOfNode($doc, $refnodename, $pos)
}
fclose($errorFile1);
fclose($sourceFile1);
mysql_close($conn);
echo "\n[$countSuccessful] Successful, [$countFailed] Failed\n";
?>