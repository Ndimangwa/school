<?php 
if (! isset($argv[1])) die("\nCommand Syntax [ php graduation.php graduationYear ]\n");
$graduationYear = $argv[1];
require_once("../../class/system.php");
$config = "../../config.php";
include($config);
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$query = "SELECT studentId FROM students";
$result = mysql_db_query($database, $query, $conn) or die("Could not fetch students");
$profile1 = null;
$graduationGroupId = null;
$count = 0;
try {
	$profile1 = new Profile($database, Profile::getProfileReference($database, $conn), $conn);
	$graduationGroupId = Group::getGroupIdFromGroupName($database, $conn, "Graduation");
} catch (Exception $e)	{
	die($e->getMessage());
}
while (list($studentId) = mysql_fetch_row($result))	{
	$student1 = null;
	$ulogin1 = null;
	$course1 = null;
	try {
		$student1 = new Student($database, $studentId, $conn);
		$ulogin1 = $student1->getLogin();
		$course1 = $student1->getCourse();
		if (is_null($course1)) Object::shootException("Course were not found");
	} catch (Exception $e)	{
		echo "*";
		continue; //Just Proceed
	}
	//We need to make sure we are dealing with finalist who are in their last semester 
	if ((intval($student1->getCurrentSemester()) == intval($profile1->getNumberOfSemestersPerYear())) && (intval($course1->getDuration()) == intval($student1->getCurrentYear()))/* && $student1->isFinalYear() && ! $student1->isAlreadyGraduated()*/)	{
		$groupName = $course1->getCourseCode();
		$groupName .= "_G_".$graduationYear; //ie MD_G_2016
		$groupId = null;
		try {
			$groupId = Group::addGroupIfNotExists($database, $conn, $groupName, $graduationGroupId, 0, 0, $course1->getCourseId(), 1);
			$ulogin1->setGroup($groupId);
			$student1->setGraduationYear($graduationYear);
			$student1->setAlreadyGraduated("1");
			$student1->commitUpdate();
			$ulogin1->commitUpdate();
			$count++;
			echo "!";
		} catch (Exception $e)	{
			echo "?";
			continue;
		}
	} else {
		echo ".";
	}
}
mysql_close($conn);
echo "\n[$count] : Successful\n";
?>