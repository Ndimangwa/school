<?php 
include("../config.php");
require_once("../class/system.php");
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
$promise1 = new Promise();
$promise1->setPromise(true);
$errorDetails = "";
$accademicYear1 = null;
try {
	$accademicYear1 = new AccademicYear($database, 1, $conn);
} catch (Exception $e)	{
	$promise1->setReason($e->getMessage());
	$promise1->setPromise(false);
}
for ($i = 2010, $counter=2; ($i <= 2099) && $promise1->isPromising(); $i++, $counter++)	{
	$updateArray = array();
	$updateArray['accademicYear'] = $i."/".($i+1);
	$updateArray['accademicYearNumber'] = $counter;
	try {
		$accademicYear1->cloneMe($updateArray);
	} catch (Exception $e)	{
		$promise1->setReason("At loop index = $i : ".$e->getMessage());
		$promise1->setPromise(false);
	}
}

if ($promise1->isPromising())	{
	echo "\nSuccessful\n";
} else {
	$errorDetail = $promise1->getReason();
	echo "\nFailed with Errors: Error Details .$errorDetail\n";
}
mysql_close($conn);
?>
