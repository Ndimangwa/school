<?php 
require_once("../../class/system.php");
include("../../config.php");
class Score	{
	private $score;
	private $i;
	private $j;
	private $selected;
	public function __construct($score, $i, $j)	{
		$this->i = $i;
		$this->score = $score;
		$this->j = $j;
		$this->selected = false;
	}
	public function getScore()	{return $this->score;}
	public function getValueOfI()	{return $this->i;}
	public function getValueOfJ()	{return $this->j;}
	public function setSelected($bln)	{$this->selected = $bln;}
	public function isSelected()	{return $this->selected;}
}
class Name	{
	private $nameStr;
	private $nameArr;
	public function __construct($name)	{
		$this->nameStr = $name;
		/* Remove non characters first */
		$name = ereg_replace("[^A-Za-z]", " ", $name);
		/* Separate at space boundary */
		$name = explode(" ", $name);
		$j = 0;	/* Inorder to control empties */
		$arr1= array();
		for ($i=0; $i<sizeof($name); $i++)	{
			/* trim the results */
			if ($name[$i] != "") $arr1[$j++] = trim($name[$i]); 
		}
		$this->nameArr = & $arr1;
	}
	public function __destruct()	{}
	public function getNameString ()	{return $this->nameStr;}
	public function getNameArray()		{return $this->nameArr;}
	public function percentageMatch($Name1)	{
		/* We are comparing a Name with another Name
			i.e 200156Ndimangwa Fadhili!2
			Method, separate in a space boundary
			Then in each word of an array remove non characters
			i.e 200156Ndimangwa should be Ndimangwa
		 */
		/*Get these two arrays*/
		if (! isset($Name1)) return 0;
		$arr1 = $this->nameArr;
		$arr2 = $Name1->getNameArray();
		/* dealing with scores */
		$scores = array();
		$len1 = sizeof($arr1);
		$len2 = sizeof($arr2);
		$k=0; 
		if ($len1 == 0 || $len2 == 0) return 0;
		for ($i=0; $i<$len1; $i++)	{
			for ($j=0; $j<$len2; $j++)	{
				$scores[$k++] = new Score($this->percentageWordMatch($arr1[$i], $arr2[$j]), $i, $j);
			}
		}
		/* Now sort these scores */
		$this->sortArrayDescending($scores);
		/* Deciding which value to take */
		$score = 0;
		for ($i=0; $i<sizeof($scores); $i++)	{
			if ($this->shouldITakeThisScore($scores, $i)) {
				$score += $scores[$i]->getScore();
				$scores[$i]->setSelected(true);
			}
			/*echo "\nValue i = ".$scores[$i]->getValueOfI()."; Value j = ".$scores[$i]->getValueOfJ()."; Scores = ".$scores[$i]->getScore()."\n";*/
		}
		/* Now the number of scores taken will be equal to the length of a small size array */
		if ($len1 > $len2) $len1 = $len2;
		$score = $score / $len1;
		return $score;
	}
	private function shouldITakeThisScore(& $scoreArray, $index)	{
		$bln = true;
		$val_i = $scoreArray[$index]->getValueOfI();
		$val_j = $scoreArray[$index]->getValueOfJ();
		/* Search from the previous list if one of the members of this pair has been taken */
		for ($i=0; $i<$index;$i++)	{
			if ((($val_i == $scoreArray[$i]->getValueOfI()) || ($val_j == $scoreArray[$i]->getValueOfJ())) && $scoreArray[$i]->isSelected())	{
				$bln = false;
				break;
			}
		}
		return $bln;
	}
	private function sortArrayDescending(& $arr1)	{
		/* Array of score objects */
		$len = sizeof($arr1);
		for ($i=0;$i<$len-1;$i++)	{
			for ($j=0;$j<$len-1-$i;$j++)	{
				if ($arr1[$j]->getScore() <  $arr1[$j+1]->getScore())	{
					$swap=$arr1[$j];
					$arr1[$j]=$arr1[$j+1];
					$arr1[$j+1]=$swap;
				}
			}
		}
	}
	private function percentageWordMatch($str1, $str2)	{
		/* Percentage of common characters over the average length */
		$score = 0;
		$t_score = 0;
		$str1 = strtolower($str1);
		$str2 = strtolower($str2);
		$len1 = strlen($str1);
		$len2 = strlen($str2);
		if ($len1 == 0 || $len2 == 0) return 0;
		for ($i=0; $i < $len1; $i++)	{
			for ($j=0; $j < $len2; $j++)	{
				$i_temp = $i;
				$j_temp = $j;
				$t_score = 0;
				if (isset($str1[$i])) {
					$charX = $str1[$i];
				} else {
					$charX=' ';
				}
				if (isset($str2[$j])) {
					$charY = $str2[$j];
				} else {
					$charY=' ';
				}
				/* Initialize this temp score */
				while (($charX == $charY) && $i<$len1 && $j<$len2)	{ /* change all to lower case */
					$t_score++;	/* keep track of matched characters */
					$i++;
					$j++;	/* Advance to next character for each string */
					if (isset($str1[$i])) {
						$charX = $str1[$i];
					} else {
						$charX=' ';
					}
					if (isset($str2[$j])) {
						$charY = $str2[$j];
					} else {
						$charY=' ';
					}
				}
				if ($t_score > $score) $score = $t_score;	/* We need the higher value */
				$i = $i_temp;	/* We need to restore the values of i and j to get the next possible match */
				$j = $j_temp;
			}
		}
		/* We need to change to a percentage, over average length */
		$len1 = ($len1 + $len2) / 2;
		$score = ($score * 100) / $len1;
		return $score;
	}
}
class Account	{
	private $code;
	private $name;
	private $group;
	private $contact;
	private $physicalAddress;
	private $postalAddress;
	private $telephone;
	private $taxNumber;
	private $registrationNumber;
	private $creditLimits;
	private $email;
	private $currency;
	private $fullname;
	private $line;
	private $found;
	private $duplicate;
	private $threshold;
	private $database;
	private $conn;
	private $xmlFolder;
	public function synchronize()	{
		$line = $this->code;
		$line .= ",".trim($this->name);
		$line .= ",".trim($this->fullname);
		$line .= ",".str_replace(",", "", trim($this->group));
		$line .= ",".str_replace(",", "", trim($this->contact));
		$line .= ",".str_replace(",", "", trim($this->physicalAddress));
		$line .= ",".str_replace(",", "", trim($this->postalAddress));
		$line .= ",".trim($this->telephone);
		$line .= ",".str_replace(",", "", trim($this->taxNumber));
		$line .= ",".trim($this->registrationNumber);
		$line .= ",".str_replace(",", "", trim($this->creditLimits));
		$line .= ",".trim($this->email);
		$line .= ",".trim($this->currency);
		$this->line = $line;
	}
	public function isFound()	{ return $this->found; }
	public function isDuplicate()	{ return $this->duplicate; }
	public function getLine()	{ return $this->line; }
	public function analyse3()	{
		$query = "SELECT studentId, login.fullname FROM students, login WHERE students.loginId=login.loginId";
		$result = mysql_db_query($this->database, $query, $this->conn) or Object::shootException("Could not fetch Analyse3");
		$student1 = null;
		$threshold = $this->threshold;
		$name1 = new Name(trim($this->name));
		$maximumAttainedScore = 0;
		while (list($studentId, $fullname)=mysql_fetch_row($result))	{
			$score = intval($name1->percentageMatch(new Name($fullname)));
			if (($score >= $threshold) && ($score > $maximumAttainedScore))	{
				$tstudent1 = new Student($this->database, $studentId, $this->conn);
				if ($tstudent1->getLogin()->isAdmitted())	{
					$maximumAttainedScore = $score;
					$student1= $tstudent1;
					$student1->loadXMLFolder($this->xmlFolder);
				}
			}
		}
		if (! is_null($student1))	{
			$login1 = $student1->getLogin();
			$temp = $student1->getListOfNextOfKins();
			if (! is_null($temp) && sizeof($temp) > 0)	{
				$this->contact = $temp[0]->getElementsByTagName('kinName')->item(0)->nodeValue;
				//$this->contact .= "(".$temp[0]->getElementsByTagName('mobile')->item(0)->nodeValue.")";
			}
			$this->physicalAddress = $student1->getPhysicalAddress();
			$this->postalAddress = $student1->getPostalAddress();
			$this->telephone = $login1->getPhone();
			$this->email = $login1->getEmail();
			$this->registrationNumber = $student1->getRegistrationNumber();
			$this->fullname = $login1->getFullname();
			$this->found = true;
		}
	}
	public function analyse2()	{
		$fullname = trim($this->name);
		$query = "SELECT studentId FROM students, login WHERE students.loginId=login.loginId AND login.fullname='$fullname'";
		$result = mysql_db_query($this->database, $query, $this->conn) or Object::shootException("Could not fetch Analyse2");
		$student1 = null;
		$recordfound = 0;
		while (list($studentId)=mysql_fetch_row($result))	{
			$student1 = new Student($this->database, $studentId, $this->conn);
			if ($student1->getLogin()->isAdmitted()) {
				$recordfound++;
			}
		}
		if ($recordfound == 1)	{
			if (! is_null($student1))	{
				$login1 = $student1->getLogin();
				$temp = $student1->getListOfNextOfKins();
				if (! is_null($temp) && sizeof($temp) > 0)	{
					$this->contact = $temp[0]->getElementsByTagName('kinName')->item(0)->nodeValue;
					$this->contact .= "(".$temp[0]->getElementsByTagName('mobile')->item(0)->nodeValue.")";
				}
				$this->physicalAddress = $student1->getPhysicalAddress();
				$this->postalAddress = $student1->getPostalAddress();
				$this->telephone = $login1->getPhone();
				$this->email = $login1->getEmail();
				$this->registrationNumber = $student1->getRegistrationNumber();
				$this->fullname = $login1->getFullname();
				$this->found = true;
			}
		} else if ($recordfound > 1)	{
			$this->duplicate = true;
		}
	}
	public function analyse()	{
		//Database 
		$query = Student::getQueryText();
		//SELECT studentId FROM students 
		$searchText = "";
		$nameArr = explode(" ", $this->name);
		$count = 0;
		foreach ($nameArr as $text)	{
			$text = trim($text);
			if ($text == "") continue;
			if ($text == ".") continue;
			if ($count == 0)	{
				$searchText = $text;
			} else {
				$searchText .= ";".$text;
			}
			$count++;
		}
		//Now Having my search text 
		$matrix1 = new SearchMatrix($searchText); //Need to be recreated per objects 
		$result = mysql_db_query($this->database, $query, $this->conn) or Object::shootException("Error $query ".mysql_error());
		$count = 0;
		while (list($studentId) = mysql_fetch_row($result))	{
			$student1 = new Student($this->database, $studentId, $this->conn);
			$student1->loadXMLFolder($this->xmlFolder);
			$login1 = $student1->getLogin();  
			if ($login1->searchMatrix($matrix1)->evaluateResult() && $login1->isAdmitted())	{
				if ($this->found) {
					$this->duplicate = true;
					$this->found = false;
					return;
				}
				//Assign  
				$temp = $student1->getListOfNextOfKins();
				if (! is_null($temp) && sizeof($temp) > 0)	{
					$this->contact = $temp[0]->getElementsByTagName('kinName')->item(0)->nodeValue;
					$this->contact .= "(".$temp[0]->getElementsByTagName('mobile')->item(0)->nodeValue.")";
				}
				$this->physicalAddress = $student1->getPhysicalAddress();
				$this->postalAddress = $student1->getPostalAddress();
				$this->telephone = $login1->getPhone();
				$this->email = $login1->getEmail();
				$this->registrationNumber = $student1->getRegistrationNumber();
				$this->fullname = $login1->getFullname();
				$this->found = true;
			}
			$count++;
		}
	}
	public function __construct($database, $conn, $xmlFolder, $line, $threshold)	{
		$this->found = false;
		$this->duplicate = false;
		$lineArr = explode(",", $line);
		if (sizeof($lineArr) < 12) Object::shootException("Could not create object line [$line]");
		$this->line = $line;
		$this->xmlFolder = $xmlFolder;
		$this->code = trim($lineArr[0]);
		$this->name = trim($lineArr[1]);
		$this->group = trim($lineArr[2]);
		$this->conn = $conn;
		$this->database = $database;	
		$this->threshold = intval($threshold);
	}
}
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database");
//My Program Start Here 
//Command syntanx php extract.php sourcefile successfile.out failedfile.out 
if (sizeof($argv) < 5) die("\nCommand Syntanx \"php extract.php sourcefile.csv success.out error.out threshold\"\n");
$inputfile = $argv[1];
$successfile = $argv[2];
$errorfile = $argv[3];
$threshold = $argv[4];
if (! file_exists($inputfile)) die("Source file not present");
$inputfile1 = fopen($inputfile, "r") or die("Could not Open $filename for reading");
$out1 = fopen($successfile, "w") or die("Could not open $successfile for writing");
$error1 = fopen($errorfile, "w") or die("Could not open $errorfile for writing");
$lineheader = "Code,Name,Sys.Name,Group,Contact,Physical.Address,Postal.Address,Telephone,Tax.Number,Registration.Number,Credit.Limit,Email,Currency\n";
fwrite($out1, $lineheader) or die("Could not write header Information in $successfile");
fwrite($error1, $lineheader) or die("Could not write header Information is $errorfile");
//Reading line by line
//Statistics collection 
$linesuccess = 0;
$linefailed = 0;
$lineerror = 0;
$lineduplicate = 0;
$linenotfound = 0;
fgets($inputfile1); //We need to skip processing header
while (($line = fgets($inputfile1)) !== false)	{
	try {
		$objLine1 = new Account($database, $conn, "../../data/students", $line, $threshold);
		$objLine1->analyse3();
		$objLine1->synchronize();
		$linetowrite = $objLine1->getLine();
		$linetowrite .= "\n";
		if ($objLine1->isFound())	{
			fwrite($out1, $linetowrite) or Object::shootException("Could not write $linetowrite to $successfile");
			echo "!";
			$linesuccess++;
		} else {
			fwrite($error1, $linetowrite) or Object::shootException("Could not write $linetowrite to $errorfile");
			if ($objLine1->isDuplicate())	{
				echo "d";
				$lineduplicate++;
			} else {
				echo ".";
				$linenotfound++;
			}
			$linefailed++;
		}
		
	} catch (Exception $e)	{
		echo "e";
		$lineerror++;
		continue;
	}
}
fclose($error1);
fclose($out1);
fclose($inputfile1);
mysql_close($conn);
echo "Completed\nSuccess Lines ($linesuccess) , Not Found Lines ($linefailed) and Error Lines ($lineerror), Duplicate Lines ($lineduplicate), Non Found Lines ($linenotfound)\n";
?>