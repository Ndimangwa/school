<?php 
include("../config.php");
require_once("../class/system.php");
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database service");
$promise1 = new Promise();
$promise1->setPromise(true);
$refStudent1 = null;
$refLogin1 = null;
try {
	$refStudent1 = new Student($database, "1", $conn);
	$refLogin1 = $refStudent1->getLogin();
} catch (Exception $e)	{
	$promise1->setReason($e->getMessage());
	$promise1->setPromise(false);
}

for ($i = 1; ($i <= 200) && $promise1->isPromising(); $i++)	{
	$updateArray = array();
	$updateArray['loginName']= "tz.student".$i;
	$updateArray['firstname'] = "student".$i;
	$updateArray['middlename'] = "midname".$i;
	$updateArray['lastname'] = "lastname".$i;
	$updateArray['fullname'] = "Test Student ".$i;
	$updateArray['email'] = "st$i@mri.ac.tz";
	$updateArray['phone'] = "+25579126".System::numberWidthCorrection($i, 4);
	$updateArray['sexId'] = ($i % 2) + 1;
	try {
		$loginId = $refLogin1->cloneMe($updateArray);
		$updateArray['loginId'] = $loginId;
		$updateArray['currentYear'] = "1";
		$updateArray['currentSemester'] = "2";
		$refStudent1->cloneMe($updateArray);
	} catch (Exception $e)	{
		$promise1->setReason("At loop index = $i : ".$e->getMessage());
		$promise1->setPromise(false);
	}
}

if ($promise1->isPromising())	{
	echo "\nSuccessful\n";
} else {
	$errorDetail = $promise1->getReason();
	echo "\nFailed with Errors: Error Details .$errorDetail\n";
}
mysql_close($conn);
?>
