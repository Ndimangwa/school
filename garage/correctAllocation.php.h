<?php 
include("../config.php");
require_once("../class/system.php");
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$query="SELECT studentId FROM students";
$result = mysql_db_query($database, $query, $conn) or die("Could not execute query");
$studentJobId = JobTitle::getStudentJobTitleId($database, $conn);
$studentGroupId = Group::getGroupIdFromGroupName($database, $conn, "Student");
$graduationGroupId = Group::getGroupIdFromGroupName($database, $conn, "Graduation");
$currentAcademicYearId = AccademicYear::getBatchIdFromName($database, $conn, "2015/2016");
$systemAccademicYear1 = new AccademicYear($database, $currentAcademicYearId, $conn);
$count = 0;
while (list($studentId) = mysql_fetch_row($result))	{
	$student1 = new Student($database, $studentId, $conn);
	$login1 = $student1->getLogin();
	if (! $login1->isAdmitted()) continue;
	if (is_null($student1->getRegistrationNumber())) continue;
	//We need only admitted in the pool of admission 
	$registrationNumber = $student1->getRegistrationNumber();
	$regArr = explode("/", $registrationNumber);
	if (sizeof($regArr) < 6) continue;
	$lyear = $regArr[2];
	$ryear = $regArr[3];
	$lArr = explode(".", $lyear);
	if (sizeof($lArr) < 2) continue;
	$lyear = $lArr[sizeof($lArr) - 1];
	$admissionAccademicYearId = $lyear."/".$ryear;
	if ($admissionAccademicYearId == "2016/2017") continue;
	$admissionAccademicYear1 = null;
	try {
		$admissionAccademicYearId = AccademicYear::getBatchIdFromName($database, $conn, $admissionAccademicYearId);
		$admissionAccademicYear1 = new AccademicYear($database, $admissionAccademicYearId, $conn);
	} catch (Exception $e)	{ continue; }
	//
	$currentYear = 1 + intval($systemAccademicYear1->getAccademicYearNumber()) - intval($admissionAccademicYear1->getAccademicYearNumber());
	$currentSemester = 2;
	//Student Group 
	$groupname = $student1->getCourse()->getCourseCode();
	$groupname = $groupname."_".$currentYear."_SEM_".$currentSemester;
	if (intval($currentYear) <= intval($student1->getCourse()->getDuration()))	{
		//Only Student who are continuing with studies
		try {
			$login1->setJobTitle($studentJobId);
			$thisStudentGroupId = Group::getGroupIdFromGroupName($database, $conn, $groupname);
			$login1->setGroup($thisStudentGroupId);
			$student1->setCurrentYear($currentYear);
			$student1->setCurrentSemester($currentSemester);
			$student1->allocateToFinalYearIfFinalYear();
			$student1->setCurrentAccademicYear($currentAcademicYearId);
			$student1->setListOfBatches($admissionAccademicYear1->getAccademicYearId());
			$student1->setAdmissionBatch($admissionAccademicYear1->getAccademicYearId());
			$login1->setPassword(sha1("12345678"));
			$login1->setUsingDefaultPassword("1");
			$username = Login::generateUsernameFor($database, $conn, $login1, 0);
		//	$login1->setLoginName($username);
		//	$login1->commitUpdate();
			$count++;
			echo "!";
		} catch (Exception $e)	{
			echo "e";
			continue;
		}
	} else {
		echo ".";
	}
}
mysql_close($conn);
echo "\n[ $count ] : Successful\n";
?>
