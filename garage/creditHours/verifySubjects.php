<?php 
if (sizeof($argv) != 2) die("\nCommand Syntax : \"php verifySubjects.php inputfile.csv\"\n");
$config = "../../config.php";
include($config);
require_once("../../class/system.php");
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
//Step 1: Holding Array 
$subjectArray1 = array();
$courseIndexArray1 = array();
$subjectCodeArray1 = array();
//Step 2: Populate Array
$filename = $argv[1];
$file1 = fopen($filename, "r") or die("Could not open filename [ $filename ]");
$linecount = 0;
while (($line = fgets($file1)) !== false)	{
	$linecount++;
	if ($linecount == 1) continue;
	$lineArr = explode(",", $line);
	if (sizeof($lineArr) < 6) continue;
	$subjectName = trim($lineArr[0]);
	$subjectCode = trim($lineArr[1]);
	//Preparing subjectCode 
	$tempArr = explode(" ", $subjectCode);
	$subjectCode = ""; foreach ($tempArr as $dt)	$subjectCode .= $dt;
	$courseName = trim($lineArr[2]);
	$year = trim($lineArr[3]);
	$semester = trim($lineArr[4]);
	$creditHours = trim($lineArr[5]);
	//********************************
	//Finding reference of course 
	$query = "SELECT courseId FROM course WHERE courseName='$courseName'";
	$result = mysql_db_query($database, $query, $conn) or die("Course [$courseName] : Could not find reference");
	if (mysql_num_rows($result) !== 1) die("Course [$courseName] : Empty or Duplicates Found");
	list($courseId)=mysql_fetch_row($result);
	//Populating Array 
	$listsize = sizeof($subjectArray1);
	$subjectArray1[$listsize] = array();
	$subjectArray1[$listsize]['subjectName'] = $subjectName;
	$subjectArray1[$listsize]['subjectCode']= $subjectCode;
	$subjectArray1[$listsize]['courseName'] = $courseName;
	$subjectArray1[$listsize]['courseId'] = $courseId;
	$subjectArray1[$listsize]['year'] = $year;
	$subjectArray1[$listsize]['semester'] = $semester;
	$subjectArray1[$listsize]['creditHours'] = $creditHours;
	$subjectCodeArray1[$subjectCode] = $listsize;
	//Another Array 
	if (! isset($courseIndexArray1[$courseId])) $courseIndexArray1[$courseId] = $courseId;
}
//Clean All Courses Un-referenced 
$query = "SELECT courseId FROM course";
$result = mysql_db_query($database, $query, $conn) or die("Could not fetch course");
while(list($courseId)=mysql_fetch_row($result))	{
	try {
		$course1 = new Course($database, $courseId, $conn);
		$course1->commitDelete();
	} catch (Exception $e)	{
		continue;
	}
}
//Clean All Subjects Un-referenced 
$query = "SELECT subjectId FROM subject";
$result = mysql_db_query($database, $query, $conn) or die("Could not fetch subjects");
while(list($subjectId)=mysql_fetch_row($result))	{
	try {
		$subject1 = new Subject($database, $subjectId, $conn);
		$subject1->commitDelete();
	} catch (Exception $e)	{
		continue;
	}
}
//Now Begin traversing each course found 
$actualSystemCodeArray1 = array();
foreach ($courseIndexArray1 as $courseId)	{
	$query = "SELECT transactionId FROM courseAndSubjectTransaction WHERE courseId='$courseId'";
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch transaction references");
	while(list($transactionId)=mysql_fetch_row($result))	{
		$transaction1 = new CourseAndSubjectTransaction($database, $transactionId, $conn);
		$subject1 = $transaction1->getSubject();
		if (is_null($subject1)) die("Reference Subject not set");
		$subjectCode = $subject1->getSubjectCode();
		$tSubjectCode = $subjectCode;
		$tempArr = explode(" ", $subjectCode);
		$subjectCode = ""; foreach ($tempArr as $dt)	$subjectCode .= $dt;
		$actualSystemCodeArray1[$subjectCode] = $tSubjectCode; //If in this Array meaning it is in the system
		$year = $transaction1->getYear();
		$semester = $transaction1->getSemester();
		$creditHours = $transaction1->getCreditHours();
		///
		if (isset($subjectCodeArray1[$subjectCode]))	{
			//Found 
			$index = $subjectCodeArray1[$subjectCode];
			$sArray1 = $subjectArray1[$index];
			$transaction1->setYear($sArray1['year']);
			$transaction1->setSemester($sArray1['semester']);
			$transaction1->setCreditHours($sArray1['creditHours']);
		} else {
			//Not Found , Mark delete, set Year = 97 
			$transaction1->setYear("97");
		}
		$transaction1->commitUpdate();
	}
}
//Now we need to Add All Subjects Which are Missing 
foreach ($subjectCodeArray1 as $subjectCode => $index)	{
	if (! isset($actualSystemCodeArray1[$subjectCode])) {
		//Then we can safely Add, It does not exists  
		// or may be subject not added but let us now add 
		$sArray1 = $subjectArray1[$index];
		$subjectName = $sArray1['subjectName'];
		$year = $sArray1['year'];
		$semester = $sArray1['semester'];
		$creditHours = $sArray1['creditHours'];
		$courseId = $sArray1['courseId'];
		$extraFilter = System::getCodeString(8);
		$subjectId = Subject::add($database, $conn, $subjectName, $subjectCode, 0, 0, 0, 0, $extraFilter, "VerifySubject Script", 0);
		$transactionId = CourseAndSubjectTransaction::add($database, $conn, $courseId, $subjectId, $creditHours * 10, 0, 0, 0, 0, 0, $creditHours, $year, $semester, 1, 0, 0, $extraFilter, "VerifySubject Script", 0);
	}	
}
fclose($file1);
mysql_close($conn);
echo "\n[ $linecount ] :Successful\n";
?>