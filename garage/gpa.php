<?php 
require_once("../class/system.php");
$config="../config.php";
include($config);
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$levelIdList = array();
$levelIdList['undergraduate'] = array();
$levelIdList['undergraduate'][sizeof($levelIdList['undergraduate'])] = 15; //Degree 
$levelIdList['postgraduate'] = array();
$levelIdList['postgraduate'][sizeof($levelIdList['postgraduate'])] = 17; //Masters
$levelIdList['postgraduate'][sizeof($levelIdList['postgraduate'])] = 21; //Doctorate Degree 
//Data Now 
$dataArray = array();
$dataArray['undergraduate'] = array();
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['awardName'] = "First Class";
$dataArray['undergraduate'][$index]['lowestGPA'] = "4.4";
$dataArray['undergraduate'][$index]['highestGPA'] = "5.1";
$dataArray['undergraduate'][$index]['extraInformation'] = "First Class";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['awardName'] = "Upper Second";
$dataArray['undergraduate'][$index]['lowestGPA'] = "3.5";
$dataArray['undergraduate'][$index]['highestGPA'] = "4.4";
$dataArray['undergraduate'][$index]['extraInformation'] = "Upper Second";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['awardName'] = "Lower Second";
$dataArray['undergraduate'][$index]['lowestGPA'] = "2.7";
$dataArray['undergraduate'][$index]['highestGPA'] = "3.5";
$dataArray['undergraduate'][$index]['extraInformation'] = "Lower Second";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['awardName'] = "Pass";
$dataArray['undergraduate'][$index]['lowestGPA'] = "2.0";
$dataArray['undergraduate'][$index]['highestGPA'] = "2.7";
$dataArray['undergraduate'][$index]['extraInformation'] = "Pass";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['awardName'] = "No Award";
$dataArray['undergraduate'][$index]['lowestGPA'] = "0.0";
$dataArray['undergraduate'][$index]['highestGPA'] = "2.0";
$dataArray['undergraduate'][$index]['extraInformation'] = "No Award";
$dataArray['undergraduate'][$index]['flags'] = "32";

$dataArray['postgraduate'] = array();
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['awardName'] = "Excellent";
$dataArray['postgraduate'][$index]['lowestGPA'] = "4.4";
$dataArray['postgraduate'][$index]['highestGPA'] = "5.1";
$dataArray['postgraduate'][$index]['extraInformation'] = "Excellent";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['awardName'] = "Very Good";
$dataArray['postgraduate'][$index]['lowestGPA'] = "3.5";
$dataArray['postgraduate'][$index]['highestGPA'] = "4.4";
$dataArray['postgraduate'][$index]['extraInformation'] = "Very Good";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['awardName'] = "Good";
$dataArray['postgraduate'][$index]['lowestGPA'] = "2.7";
$dataArray['postgraduate'][$index]['highestGPA'] = "3.5";
$dataArray['postgraduate'][$index]['extraInformation'] = "Good";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['awardName'] = "No Award";
$dataArray['postgraduate'][$index]['lowestGPA'] = "0.0";
$dataArray['postgraduate'][$index]['highestGPA'] = "2.7";
$dataArray['postgraduate'][$index]['extraInformation'] = "No Award";
$dataArray['postgraduate'][$index]['flags'] = "32";

//Now the real Business Begin 
foreach ($levelIdList as $key => $levelArray)	{
	//key undergraduate/postgraduate 
	$localDataArray = $dataArray[$key];
	foreach ($levelArray as $levelId)	{
		//You need All Grades for each levelId 
		foreach ($localDataArray as $gradeArray)	{
			$awardName = $gradeArray['awardName'];
			$lowestGPA = $gradeArray['lowestGPA'];
			$highestGPA = $gradeArray['highestGPA'];
			$extraInformation = $gradeArray['extraInformation'];
			$extraFilter = "12345678";
			$flags = $gradeArray['flags'];
			//Insert Use a safer method to do this 
			ClassOfAward::add($database, $conn, $awardName, $levelId, $lowestGPA, $highestGPA, $extraFilter, $extraInformation, $flags);
		}
	}
}
mysql_close($conn);
echo "\nSuccessful\n";
?>