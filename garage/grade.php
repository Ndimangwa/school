<?php 
require_once("../class/system.php");
$config="../config.php";
include($config);
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$levelIdList = array();
$levelIdList['undergraduate'] = array();
$levelIdList['undergraduate'][sizeof($levelIdList['undergraduate'])] = 15; //Degree 
$levelIdList['postgraduate'] = array();
$levelIdList['postgraduate'][sizeof($levelIdList['postgraduate'])] = 17; //Masters
$levelIdList['postgraduate'][sizeof($levelIdList['postgraduate'])] = 21; //Doctorate Degree 
//Data Now 
$dataArray = array();
$dataArray['undergraduate'] = array();
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "A";
$dataArray['undergraduate'][$index]['gradePoint'] = "5";
$dataArray['undergraduate'][$index]['lowestMarks'] = "80";
$dataArray['undergraduate'][$index]['highestMarks'] = "101";
$dataArray['undergraduate'][$index]['extraInformation'] = "Excellent";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "B+";
$dataArray['undergraduate'][$index]['gradePoint'] = "4";
$dataArray['undergraduate'][$index]['lowestMarks'] = "70";
$dataArray['undergraduate'][$index]['highestMarks'] = "80";
$dataArray['undergraduate'][$index]['extraInformation'] = "Very Good";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "B";
$dataArray['undergraduate'][$index]['gradePoint'] = "3";
$dataArray['undergraduate'][$index]['lowestMarks'] = "60";
$dataArray['undergraduate'][$index]['highestMarks'] = "70";
$dataArray['undergraduate'][$index]['extraInformation'] = "Good";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "C";
$dataArray['undergraduate'][$index]['gradePoint'] = "2";
$dataArray['undergraduate'][$index]['lowestMarks'] = "50";
$dataArray['undergraduate'][$index]['highestMarks'] = "60";
$dataArray['undergraduate'][$index]['extraInformation'] = "Pass";
$dataArray['undergraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "D";
$dataArray['undergraduate'][$index]['gradePoint'] = "1";
$dataArray['undergraduate'][$index]['lowestMarks'] = "40";
$dataArray['undergraduate'][$index]['highestMarks'] = "50";
$dataArray['undergraduate'][$index]['extraInformation'] = "Supplimentary";
$dataArray['undergraduate'][$index]['flags'] = "16";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "E";
$dataArray['undergraduate'][$index]['gradePoint'] = "0";
$dataArray['undergraduate'][$index]['lowestMarks'] = "0";
$dataArray['undergraduate'][$index]['highestMarks'] = "40";
$dataArray['undergraduate'][$index]['extraInformation'] = "Repeat Course/Module";
$dataArray['undergraduate'][$index]['flags'] = "80";
$index = sizeof($dataArray['undergraduate']);
$dataArray['undergraduate'][$index] = array();
$dataArray['undergraduate'][$index]['gradeName'] = "I";
$dataArray['undergraduate'][$index]['gradePoint'] = "0";
$dataArray['undergraduate'][$index]['lowestMarks'] = "1001";
$dataArray['undergraduate'][$index]['highestMarks'] = "1001";
$dataArray['undergraduate'][$index]['extraInformation'] = "Incomplete";
$dataArray['undergraduate'][$index]['flags'] = "48";

$dataArray['postgraduate'] = array();
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "A";
$dataArray['postgraduate'][$index]['gradePoint'] = "5";
$dataArray['postgraduate'][$index]['lowestMarks'] = "85";
$dataArray['postgraduate'][$index]['highestMarks'] = "101";
$dataArray['postgraduate'][$index]['extraInformation'] = "Excellent";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "B+";
$dataArray['postgraduate'][$index]['gradePoint'] = "4";
$dataArray['postgraduate'][$index]['lowestMarks'] = "75";
$dataArray['postgraduate'][$index]['highestMarks'] = "85";
$dataArray['postgraduate'][$index]['extraInformation'] = "Very Good";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "B";
$dataArray['postgraduate'][$index]['gradePoint'] = "3";
$dataArray['postgraduate'][$index]['lowestMarks'] = "60";
$dataArray['postgraduate'][$index]['highestMarks'] = "75";
$dataArray['postgraduate'][$index]['extraInformation'] = "Good";
$dataArray['postgraduate'][$index]['flags'] = "0";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "C";
$dataArray['postgraduate'][$index]['gradePoint'] = "2";
$dataArray['postgraduate'][$index]['lowestMarks'] = "50";
$dataArray['postgraduate'][$index]['highestMarks'] = "60";
$dataArray['postgraduate'][$index]['extraInformation'] = "Supplimentary";
$dataArray['postgraduate'][$index]['flags'] = "16";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "E";
$dataArray['postgraduate'][$index]['gradePoint'] = "1";
$dataArray['postgraduate'][$index]['lowestMarks'] = "0";
$dataArray['postgraduate'][$index]['highestMarks'] = "50";
$dataArray['postgraduate'][$index]['extraInformation'] = "Repeat Course/Module";
$dataArray['postgraduate'][$index]['flags'] = "80";
$index = sizeof($dataArray['postgraduate']);
$dataArray['postgraduate'][$index] = array();
$dataArray['postgraduate'][$index]['gradeName'] = "I";
$dataArray['postgraduate'][$index]['gradePoint'] = "0";
$dataArray['postgraduate'][$index]['lowestMarks'] = "1001";
$dataArray['postgraduate'][$index]['highestMarks'] = "1001";
$dataArray['postgraduate'][$index]['extraInformation'] = "Incomplete";
$dataArray['postgraduate'][$index]['flags'] = "48";
//Now the real Business Begin 
foreach ($levelIdList as $key => $levelArray)	{
	//key undergraduate/postgraduate 
	$localDataArray = $dataArray[$key];
	foreach ($levelArray as $levelId)	{
		//You need All Grades for each levelId 
		foreach ($localDataArray as $gradeArray)	{
			$gradeName = $gradeArray['gradeName'];
			$gradePoint = $gradeArray['gradePoint'];
			$lowestMarks = $gradeArray['lowestMarks'];
			$highestMarks = $gradeArray['highestMarks'];
			$extraInformation = $gradeArray['extraInformation'];
			$flags = $gradeArray['flags'];
			$extraFilter = "12345678";
			//Insert Use a safer method to do this 
			Grade::add($database, $conn, $gradeName, $gradePoint, $levelId, $lowestMarks, $highestMarks, $extraFilter, $extraInformation, $flags);
		}
	}
}
mysql_close($conn);
echo "\nSuccessful\n";
?>