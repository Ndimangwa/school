<html>
<head><title>JSON Test</title>
<link rel="stylesheet" type="text/css" media="all" href="../../client/jquery-ui-1.11.3/themes/<?= $themeFolder ?>/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../../client/plugin/printArea/PrintArea.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../../client/plugin/navgoco/jquery.navgoco.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../../client/css/purecss/pure-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../../client/css/site.css"/>
<style type="text/css">

</style>
<script type="text/javascript" src="../../client/jquery.js"></script>
<script type="text/javascript" src="../../client/jquery-ui-1.11.3/jquery-ui.js"></script>
<script type="text/javascript" src="../../client/jquery-easy-ticker-master/jquery.easy-ticker.js"></script>
<script type="text/javascript" src="../../client/plugin/printArea/jquery.PrintArea.js"></script>
<script type="text/javascript" src="../../client/plugin/navgoco/jquery.navgoco.js"></script>
<script type="text/javascript" src="../../client/js/jvalidation.js"></script>
<script type="text/javascript" src="../../client/js/page.js"></script>
<script type="text/javascript">
(function($)	{
	$.widget("ab.startx", $.ui.tooltip, {
		options: {
			
		},
		_create: function()	{
			this._super();
			this.options.content = $.proxy(this, "_content");
		},
		_content: function()	{
			var $content = $('<div/>').html("Hapa kazi tu");
			return $content;
		}
	});
	$(function()	{
		var $data1 = $('span.data-hover');
		var $target1 = $('#target');
		var obj1 = $.parseJSON($data1.attr('data-json'));
		$.each(obj1.c, function(i, v)	{
			var value = v.cc;
			window.alert(value);
		});
		$('#test1').startx();
	});
})(jQuery);
</script>
</head>
<body>

<div>
<?php 
	$dataJsonString="{\"a\": \"I am the Only A\", \"b\": [{\"bb\":\"I am the First BB\"}, {\"bb\": \"I am a Second BB\"}], \"c\": [{\"cc\": \"I am the First CC\"}, {\"cc\": \"I am Second CC\"}, {\"cc\": \"I am Third CC\"}]}";
?>
	<span class="data-hover" data-json='<?= $dataJsonString ?>'>Here is my Data Store</span>
</div>
<div id="target"></div>
<div id="test1">Testing is done here</div>
</body>
</html>