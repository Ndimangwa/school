drop database if exists udom;
create database if not exists udom;
use udom;
create table coutry	(
	countryId int(10) unsigned not null auto_increment,
	countryName varchar(48) not null,
	countryZone varchar(8) default null,
	unique (countryName),
	primary key (countryId)
) engine=InnoDB;
create table sex (
	sexId int(10) unsigned not null auto_increment,
	sexName varchar(16) not null,
	unique (sexName),
	primary key (sexId)
) engine=InnoDB;
create table marital (
	maritalId int(10) unsigned not null auto_increment,
	maritalName varchar(24) not null,
	unique (maritalName),
	primary key (maritalId)
) engine=InnoDB;
create table user (
	userId int(10) unsigned not null auto_increment,
	fullname varchar(48) not null,
	maritalId int(10) unsigned not null,
	sexId int(10) unsigned,
	foreign key (maritalId) references marital(maritalId),
	foreign key (sexId) references sex(sexId),
	primary key (userId)
) engine=InnoDB;
