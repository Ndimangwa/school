<?php 
if (sizeof($argv) != 3) die("\nCommand Syntax : \"php processData.php CourseName OutputFilename\"\n");
$config="../../config.php";
include($config);
require_once("../../class/system.php");
//fullname,sex,employed,academicYearOfJoining,dobTimeStamp,Age,CourseName,CurrentYear,CurrentSemester,isAlreadyGraduated,graduationYear
$courseName = $argv[1];
$outputFilename = $argv[2];
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$file1 = fopen($outputFilename, "w") or die("Could not Open file for writing");
$norecords = 0;
try {
	$courseId = Course::getCourseIdFromCourseName($database, $conn, $courseName);
	$query = "SELECT studentId FROM students WHERE courseId='$courseId'";
	$result = mysql_db_query($database, $query, $conn) or Object::shootException("Could not execute query");
	$lineToWrite = "fullname,sex,employed,academicYearOfJoining,dobTimeStamp,Age,CourseName,CurrentYear,CurrentSemester,isAlreadyGraduated,graduationYear\n";
	fwrite($file1, $lineToWrite) or Object::shootException("Could not Write Headers");
	while (list($studentId)=mysql_fetch_row($result))	{
		$student1 = new Student($database, $studentId, $conn);
		$ulogin1 = $student1->getLogin();
		if (is_null($ulogin1)) { echo "x"; continue; }
		if (! $ulogin1->isAdmitted()) continue;
		$sex1 = $ulogin1->getSex();
		if (is_null($sex1))	{ echo "x"; continue; }
		$dob1 = $ulogin1->getDOB();
		if (is_null($dob1)) { echo "x"; continue; }
		$academicYear1 = $student1->getAdmissionBatch();
		if (is_null($academicYear1)) { echo "x"; continue; }
		$course1 = $student1->getCourse(); //This By Default Can not be null 
		//Extracting Data 
		$fullname = $ulogin1->getFullname();
		$sex = $sex1->getSexName();
		$employed = "NO"; if ($student1->isEmployed()) $employed = "YES";
		$academicYearOfJoining = $academicYear1->getAccademicYear();
		$dobTimeStamp = $dob1->getDateAndTimeString();
		$courseName = $course1->getCourseName();
		$currentYear = $student1->getCurrentYear();
		$currentSemester = $student1->getCurrentSemester();
		$isAlreadyGraduated = "";
		$graduationYear = "";
		if ($student1->isAlreadyGraduated())	{
			$isAlreadyGraduated = "GRADUATED";
			$graduationYear = $student1->getGraduationYear();
			$currentYear = "";
			$currentSemester = "";
		}
		//Age We will derive
		$currentTime1 = new DateAndTime("Ndimangwa", Object::getCurrentTimestamp(), "Fadhili");
		$timeDiff1 = $dob1->dateDifference($currentTime1);
		$age = $timeDiff1->getYear();
		//Now Do Writing 
		$lineToWrite = "$fullname,$sex,$employed,$academicYearOfJoining,$dobTimeStamp,$age,$courseName,$currentYear,$currentSemester,$isAlreadyGraduated,$graduationYear\n";
		fwrite($file1, $lineToWrite) or Object::shootException("Could not Write Headers");
		echo "!"; $norecords++;
	}
} catch (Exception $e)	{
	fclose($file1);
	mysql_close($conn);
	die($e->getMessage());
}
fclose($file1);
mysql_close($conn);
echo "\n [ $norecords ] SUCCESSFUL\n";
?>