<?php 

	$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$student1 = null;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		try {
			$student1 = new Student($database, $_REQUEST['id'], $conn);
			//$profile1->loadXMLFolder("../data/profile");
		} catch (Exception $e)	{
			die($e->getMessage());
		}
		$storedSafePage = intval($student1->getApplicationCounter());
		$enableUpdate = false;
		//Step 1: Handling Data From the Previous Page 
		if ($prevpagenumber < $pagenumber)	{
			//$telephone = mysql_real_escape_string($_REQUEST['telephoneList']);
			if ($pagenumber -1 < $storedSafePage)	{
				//UPDATE WINDOW
				//This is the first page it does not apply
			} else {
				//INSERT WINDOW
				$student1->setCompleted("0");
				//Set if continuing or not kd788
				$student1->setContinuingStudent("0");
				if ($_REQUEST['application'] == "continuing")	$student1->setContinuingStudent("1");
				$student1->setApplicationCounter($pagenumber); 
				$enableUpdate = true;
			}
			if ($enableUpdate)	{
				try {
					$student1->commitUpdate();
				} catch (Exception $e)	{ 
					$promise1->setReason($e->getMessage());
					$promise1->setPromise(false);
				}
			}
		}
		//Step 2: Handling Data for this Page 
		//$website = "";
		$firstname = "";
		$middlename = "";
		$lastname = "";
		$fullname = "";
		$dob = "";
		$sexId = -1;
		$maritalId = -1;
		$disability = "";
		$login1 = null;
		if ($pagenumber < $storedSafePage)	{
			//$website = $profile1->getWebsite()
			$login1 = $student1->getLogin();
			$firstname = $login1->getFirstname();
			$middlename = $login1->getMiddlename();
			$lastname = $login1->getLastname();
			$fullname = $login1->getFullname();
			$dob = DateAndTime::convertFromDateTimeObjectToGUIDateFormat($login1->getDOB());
			$sexId = $login1->getSex()->getSexId();
			$maritalId = $login1->getMarital()->getMaritalId();
			$disability = $student1->getDisability();
		}
		//Beginning UI 
?>
		<div class="mobile-collapse ui-sys-panel-container">
			<div class="ui-sys-panel ui-sys-data-capture ui-widget ui-widget-content">
				<div class="ui-sys-panel-header ui-widget-header">Student Bio Data<br />App Ref No: <b><?= $student1->getReferenceNumber() ?></b></div>
				<div class="ui-sys-panel-body">
					<div>
<?php 
	if ($promise1->isPromising())	{
?>
						<form class="pure-form pure-form-aligned" id="form1" method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
							<input type="hidden" name="id" value="<?= $student1->getStudentId() ?>"/>
							<input type="hidden" name="prevpagenumber" value="<?= $pagenumber ?>"/>
							<input type="hidden" name="pagenumber" value="<?= $pagenumber + 1 ?>"/>
							<input type="hidden" name="application" value="<?= $_REQUEST['application'] ?>"/>
							<fieldset>
								<div class="pure-control-group">
									<label for="firstname">Firstname </label>
									<input type="text" name="firstname" id="firstname" value="<?= $firstname ?>" size="32" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Firstname : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="middlename">Middlename </label>
									<input type="text" name="middlename" id="middlename" value="<?= $middlename ?>" size="32" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Middlename : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="lastname">Lastname </label>
									<input type="text" name="lastname" id="lastname" value="<?= $lastname ?>" size="32" required pattern="<?= $exprA32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA32Name ?>" validate_message="Lastname : <?= $msgA32Name ?>"/>
								</div>
								<div class="pure-control-group" title="Your Name as it appears on your Academic Certificates">
									<label for="fullname">Name in Certificate </label>
									<input type="text" name="fullname" id="fullname" size="32" value="<?= $fullname ?>" required pattern="<?= $exprA64Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprA64Name ?>" validate_message="Accademic Name : <?= $msgA64Name ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="dob">Date of Birth </label>
									<input class="datepicker" type="text" name="dob" id="dob" value="<?= $dob ?>" size="32" required pattern="<?= $exprDate ?>" validate="true" validate_control="text" validate_expression="<?= $exprDate ?>" validate_message="Date: <?= $msgDate ?>"/>
								</div>
								<div class="pure-control-group">
									<label for="sexId">Sex </label>
									<select id="sexId" name="sexId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Sex">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Sex::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($login1) && ! is_null($login1->getSex()) && ($alist1['id'] == $login1->getSex()->getSexId())) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="maritalId">Marital Status</label>
									<select id="maritalId" name="maritalId" validate="true" validate_control="select" validate_expression="select" validate_message="Kindly Select Marital">
										<option value="_@32767@_">--select--</option>
<?php 
	$list1 = Marital::loadAllData($database, $conn);
	foreach ($list1 as $alist1)	{
		$selected = "";
		if (! is_null($login1) && ! is_null($login1->getMarital()) && ($alist1['id'] == $login1->getMarital()->getMaritalId())) $selected="selected";
?>
			<option <?= $selected ?> value="<?= $alist1['id'] ?>"><?= $alist1['val'] ?></option>
<?php
	}
?>
									</select>
								</div>
								<div class="pure-control-group">
									<label for="disability">Disability </label>
									<input type="text" name="disability" id="disability" value="<?= $disability ?>" size="32" notrequired="true" pattern="<?= $exprL32Name ?>" validate="true" validate_control="text" validate_expression="<?= $exprL32Name ?>" validate_message="Disability : <?= $msgL32Name ?>"/>
								</div>
								<div class="pure-controls">
									<span id="perror" class="ui-sys-error-message"></span>
								</div>
								<div class="pure-controls">
									<span class="ui-sys-inline-controls-right">
										<input id="cmd1" type="button" value="Next &gt;&gt;"/>
									</span>
								</div>
							</fieldset>
						</form>
<?php 
	} else {
?>
		<div class="ui-state-error">
			There were problems in updating some of your details<br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	} 
?>
					</div>
				</div>
				<div class="ui-sys-panel-footer"></div>
			</div>
		</div>
		<div class="ui-sys-clear-both"></div>
<script type="text/javascript">
(function($)	{
	$('#cmd1').on('click', function(event)	{
		event.preventDefault();
		generalFormSubmission(this, 'form1', 'perror');
	});
})(jQuery);
</script>
<?php
		mysql_close($conn);
?>