<?php 
$config="../../config.php";
include($config);
require_once("../../class/system.php");
$database = "dbnacte";
if (sizeof($argv) < 3) die("\nCommand Syntax \"php process.php in.csv out.csv\"\n");
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");

$filename=$argv[1];
$linenumber = 0;
$file1 = fopen($filename, "r") or die("Could not open a file for reading");
$filename = $argv[2];
$file2 = fopen($filename, "w") or die("Could not open a file for writing");
$numberOfColumns = 0;
$numberOfSubjects = 0;
while (($line = fgets($file1)) !== false)	{
	$line = str_replace("\n", "", str_replace("\r", "", $line));
	$lineArr = explode(",", $line);
	if (sizeof($lineArr) < $numberOfColumns) continue;
	$linenumber++;
	if ($linenumber == 1)	{
		//General Header
		$numberOfColumns = sizeof($lineArr);
		$numberOfSubjects = ($numberOfColumns - 2) / 2;
		$lineToWrite = $line; for ($i=0; $i<$numberOfSubjects; $i++) $lineToWrite.",,"; $lineToWrite .= ",,,"; //
		$lineToWrite .= "\n";
		fwrite($file2, $lineToWrite) or die("Could not write $lineToWrite");
	} else if ($linenumber == 2)	{
		//Subject Header
		$lineToWrite = $lineArr[0].",".$lineArr[1];
		for ($i=2; $i < $numberOfColumns; $i++)	{
			if (($i % 2) == 0) {
				$lineToWrite .= ",".$lineArr[$i];
			} else {
				$lineToWrite .= ",".$lineArr[$i].",,";
			}
		}
		$lineToWrite .= ",,,";
		$lineToWrite .= "\n";
		fwrite($file2, $lineToWrite) or die("Could not write $lineToWrite");
	} else if ($linenumber == 3)	{
		//Marks & Credit Hours 
		$lineToWrite = $lineArr[0].",".$lineArr[1];
		for ($i=2; $i < $numberOfColumns; $i++)	{
			if (($i % 2) == 0) {
				$lineToWrite .= ",".$lineArr[$i];
			} else {
				$lineToWrite .= ",".$lineArr[$i].",GPEq,GP";
			}
		}
		$lineToWrite .= ",Total Credit Hours,Total GP,GPA";
		$lineToWrite .= "\n";
		fwrite($file2, $lineToWrite) or die("Could not write $lineToWrite");
	}  else {
		//Data
		$totalCreditHours = 0.0;
		$totalGP = 0.0;
		$gpa = 0.0;
		$lineToWrite = $lineArr[0].",".$lineArr[1];
		$gpaEq = 0.0;
		for ($i=2; $i < $numberOfColumns; $i++)	{
			if (($i % 2) == 0) {
				$marks = trim($lineArr[$i]);
				$query="SELECT GPEq FROM gpaEqLookup WHERE marks='$marks'";
				$result = mysql_db_query($database, $query, $conn) or die("[$linenumber] Could not Extract $marks from gpaEqLookup");
				if (mysql_num_rows($result) != 1) die("[$linenumber] Empty or Multiple records for marks $marks in line $line 	");
				list($gpaEq)=mysql_fetch_row($result);
				$lineToWrite .= ",".$marks;
			} else {
				$creditHours = trim($lineArr[$i]);
				$GP = Number::truncate($creditHours * $gpaEq, 4);
				$lineToWrite .= ",".$creditHours.",$gpaEq,$GP";
				$totalCreditHours += $creditHours;
				$totalGP += $GP;
			}
		}
		$gpa = Number::truncate($totalGP / $totalCreditHours, Object::$__GPA_PRECISION);
		$lineToWrite .= ",$totalCreditHours,$totalGP,$gpa";
		$lineToWrite .= "\n";
		fwrite($file2, $lineToWrite) or die("Could not write $lineToWrite");
	}
}
fclose($file2);
fclose($file1);
mysql_close($conn);
echo "\n[$linenumber] Successful\n";
?>