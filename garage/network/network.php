<?php 
if (sizeof($argv) != 4) die("\nCommand Syntax \"php network.php ip_address subnet_mask test_ip_address\"\n");
require_once("../../class/system.php");
$network1 = null;
try {
	$network1 = new Network($argv[1], $argv[2]);
} catch (Exception $e)	{
	die($e->getMessage());
}
echo "\nIp Address is ".$network1->getIpAddress();
echo "\nSubnet Mask is ".$network1->getSubnetMask();
$bitString = Network::convertIpAddressToBitString($network1->getSubnetMask());
echo "\nBit Strings is ".$bitString;
echo "\nNetwork Address is ".$network1->getNetworkAddress();
echo "\nBroadcast Address is ".$network1->getBroadcastAddress();
$ipInMyNetwork = $network1->isThisIpAddressBelongToMyNetwork($argv[3]);
if ($ipInMyNetwork)	{
	echo "\nIP Address do belong to my Network";
} else {
	echo "\nIP Address DOES NOT Belong to my Network";
}
echo "\n";
?>