<?php 
require '../../vendor/autoload.php';
require '../../class/system.php';
use mikehaertl\wkhtmlto\Pdf;
$config = "../../config.php";
include($config);
//$pdf1 = new WkHtmlToPdf
$pdf1 = new Pdf();
//Creating Data for Transcript
$windowToDisplay = null;
$conn = null;
try {
	$conn = mysql_connect($hostname, $user, $pass) or Object::shootException("Could not connect to database server");
	$profile1 = new Profile($database, Profile::getProfileReference($database, $conn), $conn);
	$dataFolder = "../../data/";
	$paper1 = Paper::createPaper("210", "297");
	$horizontalScale = 0.05;
	$controlFlags = 0;
	$controlFlags = Object::setControlFlagsAt($controlFlags, Paper::$__PAPER_FLIP_ORIENTATION);
	$studentId = 973;
	$windowToDisplay = Transcript::getStudentTranscript($database, $conn, $profile1, $studentId, $paper1, $dataFolder, $horizontalScale, $controlFlags);
	if (is_null($windowToDisplay)) Object::shootException("The Transcript viewer returned an empty Transcript");
	mysql_close($conn);	
} catch (Exception $e)	{
	if (! is_null($conn)) mysql_close($conn);
	die($e->getMessage());
}
$mypage = "<html><head><meta name=\"pdfkit-pag_size\" content=\"Letter\"/><meta name=\"pdfkit-margin_top\" content=\"0.25in\"/><meta name=\"pdfkit-margin_right\" content=\"0.25in\"/><meta name=\"pdfkit-margin_bottom\" content=\"0.25in\"/><meta name=\"pdfkit-margin_left\" content=\"0.25in\"/>Transcript</head><body><div>".$windowToDisplay."</div></body></html>";
//End.Creating Data For Transcript
//$mypage = "<html><head><title>My First PDF</title></head><body><div>It is Good to be here</div></body></html>";
$pdf1->addPage($mypage);
//$pdf1->addPage('http://10.115.0.3');
/*if ($pdf1->send()) echo "SUCCESSFUL";
else echo "FAILED";*/
if ($pdf1->send())	{
	echo "SUCCESSFUL";
} else {
	echo "FAILED";
}
?>