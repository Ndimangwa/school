<?php 
require_once("../../class/system.php");
$config="../../config.php";
include($config);
class Matrix	{
	private $subjectCode;
	private $subjectName;
	private $lectureHours;
	private $seminarAndTutorialHours;
	private $assignmentHours;
	private $independentStudiesAndResearchHours;
	private $practicalHours;
	private $creditHours;
	private $year;
	private $semester;
	private $line;
	public function getSubjectCode()	{ return $this->subjectCode; }
	public function getSubjectName()	{ return $this->subjectName; }
	public function getLectureHours()	{ return $this->lectureHours; }
	public function getSeminarAndTutorialHours()	{ return $this->seminarAndTutorialHours; }
	public function getAssignmentHours()	{ return $this->assignmentHours; }
	public function getIndependentStudiesAndResearchHours()	{ return $this->independentStudiesAndResearchHours; }
	public function getPracticalTrainingHours()	{ return $this->practicalHours; }
	public function getCreditHours()	{ return $this->creditHours; }
	public function getYear()	{ return $this->year; }
	public function getSemester()	{ return $this->semester; }
	public function getLine()	{ return $this->line; }
	public function __construct($line)	{   
		//Undo !comma!
		$lineArr = explode(",", $line);
		if (sizeof($lineArr) < 11) return;
		$this->line = $line;
		$this->subjectCode = trim(str_replace("!comma!", ",", $lineArr[0]));
		$this->subjectName = trim(str_replace("!comma!", ",", $lineArr[1]));
		$this->lectureHours = trim($lineArr[2]);
		$this->seminarAndTutorialHours = trim($lineArr[3]);
		$this->practicalHours = trim($lineArr[6]);
		$this->independentStudiesAndResearchHours = trim($lineArr[5]);
		$this->assignmentHours = trim($lineArr[4]);
		$this->creditHours = trim($lineArr[8]);
		$this->year = trim($lineArr[9]);
		$this->semester = trim($lineArr[10]);
		//Doing correction 
		if (($this->lectureHours == "-") || ($this->lectureHours == "")) $this->lectureHours = 0;
		if (($this->seminarAndTutorialHours == "-") || ($this->seminarAndTutorialHours == "")) $this->seminarAndTutorialHours = 0;
		if (($this->practicalHours == "-") || ($this->practicalHours == "")) $this->practicalHours = 0;
		if (($this->independentStudiesAndResearchHours == "-") || ($this->independentStudiesAndResearchHours == "")) $this->independentStudiesAndResearchHours = 0;
		if (($this->assignmentHours == "-") || ($this->assignmentHours == "")) $this->assignmentHours = 0;
	}
}
if (! (isset($argv[1]) && isset($argv[2]))) die("\nCommand Syntax [ php programMatrix.php \"Course Name\" datafile.csv ]\n");
$conn = mysql_connect($hostname, $user, $pass) or die("\nCould not connect to a database services\n");
$courseName = mysql_real_escape_string($argv[1]);
$filename = $argv[2];
if (! file_exists($filename)) die("\nData file does not exists\n");
$linenumber = 0;
$courseId = null;
try {
	$courseId = Course::getCourseIdFromCourseName($database, $conn, $courseName);
} catch(Exception $e)	{
	$message = $e->getMessage();
	die("\n$message\n");
}
$file1 = fopen($filename, "r") or die("\nCould not open a file for reading\n");
while (($line = fgets($file1)) !== false)	{
	$linenumber++;
	if ($linenumber == 1) continue; //Skip Header
	$matrix1 = new Matrix($line);
	if (is_null($matrix1->getLine())) continue;
	//We need to find a reference subject
	$subjectId = null;
	try {
		$subjectId = Subject::getSubjectIdFromSubjectCode($database, $conn, $matrix1->getSubjectCode());
	} catch (Exception $e)	{
		//This subjectId was not found , insert a new on eand capture 
		try {
			$subjectId = Subject::add($database, $conn, $matrix1->getSubjectName(), $matrix1->getSubjectCode(), 0, 0, 0, 0, System::getCodeString(8), $matrix1->getSubjectName(), 15);
		} catch (Exception $e)	{
			echo "."; 
			continue;
		}
	}
	//We now need to update the transaction map 
	try {
		$subject1 = new Subject($database, $subjectId, $conn);
		//Add Only if no matching map 
		try {
			CourseAndSubjectTransaction::getTransactionForCourseSubjectYearAndSemester($database, $conn, $courseId, $subjectId, $matrix1->getYear(), $matrix1->getSemester());
		} catch (Exception $e)	{
			//No Matching Map , now add
			CourseAndSubjectTransaction::add($database, $conn, $courseId, $subjectId, $matrix1->getLectureHours(), $matrix1->getSeminarAndTutorialHours(), $matrix1->getAssignmentHours(), $matrix1->getIndependentStudiesAndResearchHours(), $matrix1->getPracticalTrainingHours(), 0, $matrix1->getCreditHours(), $matrix1->getYear(), $matrix1->getSemester(), 1, 0, 0, System::getCodeString(8), $subject1->getSubjectName(), 31);
		}
	} catch (Exception $e)	{
		echo ".";
		continue;
	}
	echo "!";
}
fclose($file1);
mysql_close($conn);
echo "\n[$linenumber] : Successful\n";
?>