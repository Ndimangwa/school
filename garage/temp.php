<?php 
$config = "../config.php";
require_once("../class/system.php");
include($config);
$controlName = "data";
$controlIndex = "17";
$flags = "65";
$positionalCaptions = array();
$positionalCaptions[0] = "Position 0";
$positionalCaptions[1] = "Position 1";
$positionalCaptions[2] = "Position 2";
$positionalCaptions[3] = "Position 3";
$positionalCaptions[4] = "Position 4";
$positionalCaptions[5] = "Position 5";
$positionalCaptions[6] = "Position 6";
$positionalCaptions[7] = "Position 7";
$positionalCaptions[8] = "Position 8";
$positionalCaptions[9] = "Position 9";
$defaultCaption = "Default";
$labelSize = "4";
$window1 = Object::getFlagsUI($controlName, $controlIndex, $flags, $positionalCaptions, $defaultCaption, $labelSize);
?>
<html><head><title>Flags Processor</title>
<link rel="stylesheet" type="text/css" media="all" href="../client/jquery-ui-1.11.3/themes/sunny/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/twbsPagination/twbsPagination.min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/wickedpicker/wp/wickedpicker.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/printArea/PrintArea.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/fonts/font-awesome.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/fileMenu/fileMenu.min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/purecss/pure-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/purecss/grids-responsive-min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/plugin/chromose/chromoselector-2.1.8/chromoselector.css"/>
<link rel="stylesheet" type="text/css" media="all" href="../client/css/site.css"/>
<script type="text/javascript" src="../client/jquery.js"></script>
<script type="text/javascript" src="../client/jquery-ui-1.11.3/jquery-ui.js"></script>
<script type="text/javascript" src="../client/jquery-easy-ticker-master/jquery.easy-ticker.js"></script>
<script type="text/javascript" src="../client/plugin/twbsPagination/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" src="../client/plugin/wickedpicker/wp/wickedpicker.js"></script>
<script type="text/javascript" src="../client/plugin/printArea/jquery.PrintArea.js"></script>
<script type="text/javascript" src="../client/plugin/fileMenu/fileMenu.js"></script>
<script type="text/javascript" src="../client/plugin/chromoselector-2.1.8/chromoselector.min.js"></script>
<script type="text/javascript" src="../client/js/jvalidation.js"></script>
<script type="text/javascript" src="../client/js/page.js"></script>
<script type="text/javascript">
(function($)	{
	$(function()	{
	
	});
})(jQuery);
</script>
</head><body>

<?= $window1 ?>
</body></html>