<html>
<head><title>Dynamic Table</title></head>
<body>
<?php 
	if (isset($_REQUEST['ui']))	{
		$window1 = "";
		if ($_REQUEST['ui'] == "table")	{
			$row = 0;
			$col = 0;
			$text = "";
			$header = "";
			if (isset($_REQUEST['row'])) $row = $_REQUEST['row'];
			if (isset($_REQUEST['col'])) $col = $_REQUEST['col'];
			if (isset($_REQUEST['text'])) $text = $_REQUEST['text'];
			if (isset($_REQUEST['header'])) $header = $_REQUEST['header'];
			$row = intval($row);
			$col = intval($col);
			$window1 .= "<table style=\"border-collapse: collapse;\"><thead><tr>";
			for ($i=0; $i< $col; $i++)	{
				$j = $i+1;
				$window1 .= "<th style=\"padding: 1px; border: 1px solid black;\">$header $j</th>";
			}
			$window1 .= "</tr></thead><tbody>";
			for ($i=0; $i< $row; $i++)	{
				$window1 .= "<tr>";
				for ($j =0; $j < $col; $j++)	{
					$count = ($i * $col) + $j + 1;
					$window1 .= "<td style=\"padding: 1px; border: 1px solid black;\">$text $count</td>";
				}
				$window1 .= "</tr>";
			}
			$window1 .= "</tbody></table>";
		}
		echo "<br/>$window1<br/>";
	}
?>
<!--BEGIN : Beginning of UI-->
<div>
	<form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
		<input type="hidden" name="ui" value="table"/>
		<table>
			<tr>
				<td>Enter Number of Rows		:</td>
				<td><input type="text" size="32" name="row"/></td>
			</tr>
			<tr>
				<td>Enter Number of Columns		:</td>
				<td><input type="text" size="32" name="col"/></td>
			</tr>
			<tr>
				<td>Enter Header Text	:</td>
				<td><input type="text" size="32" name="header"/></td>
			</tr>
			<tr>
				<td>Enter Cell Text		:</td>
				<td><input type="text" size="32" name="text"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Draw Table"/></td>
			</tr>
		</table>
	</form>
</div>
<!--END : Ending of UI-->
</body>
</html>