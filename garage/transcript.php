<?php 
	class TranscriptFile	{
	private $headers;
	private $dataLines;
	private $database;
	private $conn;
	private $filename;
	static $__IS_CLONE_MODE = "is_clone_mode";
	public final static function getTranscriptForStudent($database, $conn, $profile1, $student1, $systemTime1, $resultsFolder, $transcriptFolder, $headerDataArray1)	{
		$login1 = $student1->getLogin();
		if (is_null($login1)) Object::shootException("The Login Account for student were not found");
		$course1 = $student1->getCourse();
		if (is_null($course1)) Object::shootException("There were no reference Course found");
		$department1 = $course1->getDepartment();
		if (is_null($department1)) Object::shootException("There were no reference Department found");
		$level1 = $course1->getEducationLevel();
		if (is_null($level1)) Object::shootException("There is no reference education level found");
		$filename = $transcriptFolder.$student1->getTranscriptFile();
		if (is_null($student1->getTranscriptFile()) || ! file_exists($filename))	{
			//Better create it 
			$transcriptFile1 = TranscriptFile::getTranscriptFileForStudent($database, $conn, $profile1, $student1->getStudentId(), $systemTime1, $resultsFolder, $transcriptFolder);
			if (is_null($transcriptFile1)) Object::shootException("Transcript File could not be generated");
			$promise1 = $transcriptFile1->save();
			if (! $promise1->isPromising()) Object::shootException("Could not save transcript data");
		}
		$student1 = new Student($database, $student1->getStudentId(), $conn); //same as reload 
		if (is_null($student1)) Object::shootException("Student Could not be found");
		if (is_null($student1->getTranscriptFile())) Object::shootException("There were no transcript file found");
		$filename = $transcriptFolder.$student1->getTranscriptFile();
		if (md5_file($filename) != $student1->getTranscriptHash())	Object::shootException("Transcript Data has been compromised");
		$transcriptFile1 = new TranscriptFile($database, $filename, $conn);
		$window1 = "<div class=\"ui-sys-transcript\">";
		$window1 .= "<div class=\"ui-sys-printable-block ui-sys-printable-header\"><div class=\"ui-sys-profile-logo\"><img alt=\"PF\" src=\"".$profile1->getExtraFilter()."\"/></div><div class=\"ui-sys-header-content\">Transcript Data</div><div class=\"ui-sys-user-logo\"><img alt=\"SP\" src=\"".$student1->getExtraFilter()."\"/></div><div class=\"ui-sys-clearboth\">&nbsp;</div></div>";
		$window1 .= "<div class=\"ui-sys-printable-block\"><table style=\"font-size: 1em; width: 100%;\" class=\"pure-table\"><thead><tr><th colspan=\"4\"></th></tr></thead><tbody>";
		$window1 .= "<tr><td>COURSE:</td><td><b>".$course1->getCourseName()."</b></td><td>DEPARTMENT:</td><td><b>".$department1->getDepartmentName()."</b></td></tr>";
		$window1 .= "<tr><td>SPECIALIZATION:</td><td><b>".$department1->getDepartmentName()."</b></td><td>GENDER:</td><td><b>".$login1->getSex()->getSexName()."</b></td></tr>";
		$window1 .= "<tr><td>STUDENT REG NO.</td><td><b>".$student1->getRegistrationNumber()."</b></td><td>NAME:</td><td><b>".$login1->getFullname()."</b></td></tr>";
		$window1 .= "</tbody></table></div>";
		$duration = $transcriptFile1->getDuration(); 
		for ($i = 1; $i < $duration; $i++)	{
			$currentYearTranscriptFile1 = $transcriptFile1->cloneMe(); //retain original copy
			$currentYearTranscriptFile1 = $currentYearTranscriptFile1->getTranscriptFileForYear($i);
			$headers = $currentYearTranscriptFile1->getHeaders();
			$dataLines = $currentYearTranscriptFile1->getDataLines();
			$firstRow = true;
			$window1 .= "<div class=\"ui-sys-printable-block\"><table class=\"pure-table\"><thead><tr><th>YEAR(LEVEL)</th><th>MODULE CODE & TITLE</th><th>GRADE</th><th>GRADE<br/>POINTS</th><th>CREDITS</th><th>MODULE POINTS</th></tr></thead><tbody>";
			$totalUnits = 0;
			$totalPoints = 0;
			foreach ($dataLines as $dataLine1)	{
				$window1 .= "<tr>";
				if ($firstRow) {
					$window1 .= "<td rowspan=\"".(intval($currentYearTranscriptFile1->getLength()) + 2)."\">".$level1->getLevelName()." (".$level1->getLevelCode().")</td>";
				}
				$units = $dataLine1->getTotalUnits();
				$points = intval($units) * intval($dataLine1->getGradePoint());
				$window1 .= "<td>".$dataLine1->getSubjectCode().": ".$dataLine1->getSubjectName()."</td>";
				$window1 .= "<td>".$dataLine1->getGradeName()."</td>";
				$window1 .= "<td>".$dataLine1->getGradePoint()."</td>";
				$window1 .= "<td>".$units."</td>";
				$window1 .= "<td>".$points."</td>";
				$window1 .= "</tr>";
				$firstRow = false;
				$totalUnits += $units;
				$totalPoints += $points;
			}
			//ADD Two rows here
			$window1 .= "<tr><td class=\"ui-sys-inline-controls-right\" colspan=\"3\"><b>Total</b></td><td>$totalUnits</td><td>$totalPoints</td></tr>";
			$lastRow = "<tr><td colspan=\"5\">&nbsp;</td></tr>";
			if ($totalUnits != 0)	{
				$totalGPA = $totalUnits / $totalPoints;
				$awardId = ClassOfAward::getClassOfAwardFromGPAAndEducationLevel($database, $conn, $totalGPA, $level1->getLevelId());
				$totalGPA = Number::displayFloatDecimal($totalGPA, Object::$__GPA_PRECISION);
				$award1 = new ClassOfAward($database, $awardId, $conn);
				$awardName = $award1->getAwardName();
				$lastRow = "<tr><td class=\"ui-sys-inline-controls-right\" colspan=\"5\"><b>$awardName, GPA is $totalGPA</b></td></tr>";
			} 
			$window1 .= "</tbody></table></div>";
		}
		$window1 .= "</div>";
		return $window1;
	}
	public function __construct($database, $filename, $conn)	{
		if ($filename == TranscriptFile::$__IS_CLONE_MODE) return;
		if (! file_exists($filename)) Object::shootException("Transcript File, Transcript file not available");
		$file1 = fopen($filename, "r") or Object::shootException("Transcript File, Could not open file for reading");
		$this->filename = $filename;
		$this->database = $database;
		$this->conn = $conn;
		$this->headers = array();
		$this->dataLines = array();
		$lineCount = 0;
		$state = 0;
		while (($line = fgets($file1)) !== false)	{
			$lineCount++;
			$lineArr = explode(",", $line);
			if (sizeof($lineArr) != Object::$numberOfTranscriptFileColumns) continue;
			if (($state == 0) && (trim($lineArr[0]) == "@-header"))	{
				//HEADER AREA
				$keyAndValue = explode("_!_",$lineArr[1]); 
				if (sizeof($keyAndValue) != 2) continue;
				$key = $keyAndValue[0];
				$val = $keyAndValue[1];
				$this->headers[$key] = $val; 
			} else if (($state == 0) && (trim($lineArr[0]) == "@-business-logic-begin"))	{
				//TRANSITION AREA
				$state = 1;
			} else if (($state == 1) && (trim($lineArr[0]) == "@-business-logic-end"))	{
				//TRANSITION AREA TO EXIT
				$state = 2;				
			} else if ($state == 1)	{
				//DATALINES 
				$this->dataLines[sizeof($this->dataLines)] = new TranscriptDataLine($line);
			} else {
				continue;
			}			
		} //end-while
		fclose($file1);
		//The Algorithm should exit at state = 2 
		if ($state != 2) Object::shootException("Transcript file was not formed properly");
	}
	public function __destruct()	{ }
	public function getLength()	{ return sizeof($this->dataLines); }
	public function getTranscriptFileForYear($year)	{
		$dataLines = array();
		foreach ($this->dataLines as $dataLine1)	{
			if (intval($dataLine1->getYear()) == intval($year))	{
				$dataLines[sizeof($dataLines)] = $dataLine1->cloneMe();
			}
		}
		$this->dataLines = $dataLines;
		return $this;
	}
	public function getTranscriptFileForSemester($semester)	{
		$dataLines = array();
		foreach ($this->dataLines as $dataLine1)	{
			if (intval($dataLine1->getCummulativeSemester()) == intval($semester))	{
				$dataLines[sizeof($dataLines)] = $dataLine1->cloneMe();
			}
		}
		$this->dataLines = $dataLines;
		return $this;
	}
	public function getHeaders()	{ return $this->headers; }
	public function getDataLines()	{ return $this->dataLines; }
	public final static function getTranscriptFileForStudent($database, $conn, $profile1, $studentId, $systemTime1, $resultsFolder, $transcriptFolder)	{
		$student1 = new Student($database, $studentId, $conn);
		if ($student1->isDiscontinued()) Object::shootException("Transcript is for non Discontinued Students");
		if (! $student1->isAlreadyGraduated()) Object::shootException("Transcript is for students who has graduated only");
		$course1 = $student1->getCourse();
		if (is_null($course1)) Object::shootException("Student is not linked to any course");
		$level1 = $course1->getEducationLevel();
		if (is_null($level1)) Object::shootException("There is no defined education level");
		//source data Structure begin
		$subjectDataStructure1 = $student1->getMySubjectListDataStructure($profile1); /* list[cummulativeSemester][index] = transactionId */
		if (is_null($subjectDataStructure1)) Object::shootException("Could not load subject list for student");
		$resultsDataStructure1 = $student1->getMyCompiledResultsDataStructure($profile1, $resultsFolder);
		/*
			list[cummulativeSemester][index]['transactionId'] = transactionId
			list[cummulativeSemester][index]['rawMarks'] = marks [ie 80]
			list[cummulativeSemester][index]['gradeId'] = gradeId
			list[cummulativeSemester][index][transactionId] = gradeId 
		*/
		if (is_null($resultsDataStructure1)) Object::shootException("1--Could not load results for student");
		$gpaDataStructure1 = $student1->getMyGPADataStructure($subjectDataStructure1, $resultsDataStructure1);
		/*
			ds1[cummulativeSemester]['gpa'] = GPA
									['cleared'] = true //1 means cleared 0 failed 
									['awardId'] = null; reference to ClassOfAward
									['totalUnits'] = 0.0 
									['totalModulePoints'] = 0.0 
		*/
		if (is_null($gpaDataStructure1)) Object::shootException("Could not load GPA structure for student");
		$overalGPADataStructure1 = $student1->getMyOveralGPADataStructure($gpaDataStructure1);
		/*
			list['gpa'], ['cleared'], ['awardId'], ['disqualified']
		*/
		if (is_null($overalGPADataStructure1)) Object::shootException("Could not load the overall GPA structure for student");
		//source data Structure ends
		$transcriptFile1 = new TranscriptFile($database, TranscriptFile::$__IS_CLONE_MODE, $conn);
		//Writing headers 
		$headers = array();
		$headers['studentId'] = $studentId;
		$headers['compilationTime'] = $systemTime1->getDateAndTimeString();
		$headers['fileType'] = "transcript";
		$headers['fileTypeCode'] = md5($headers['fileType']);
		//Student Course 
		$course1 = $student1->getCourse();
		if (is_null($course1)) Object::shootException("Reference Course for Student were not found");
		$headers['courseId'] = $course1->getCourseId();
		$headers['courseName'] = $course1->getCourseName();
		$headers['duration'] = $course1->getDuration();
		$headers['gpa'] = $overalGPADataStructure1['gpa'];
		$cleared = 0;
		if ($overalGPADataStructure1['cleared']) $cleared = 1;
		$headers['cleared'] = $cleared;
		$disqualified = 0;
		if ($overalGPADataStructure1['disqualified']) $disqualified = 1;
		$headers['disqualified'] = $disqualified;
		//Award 
		$headers['awardId'] = $overalGPADataStructure1['awardId'];
		if (! is_null($headers['awardId']))	{
			$award1 = new ClassOfAward($database, $headers['awardId'], $conn);
			$headers['awardName'] = $award1->getAwardName();
		}
		$headers['graduationYear'] = $student1->getGraduationYear();
		//Now dealing with data 
		$dataLines = array();
		$yearDataArray1 = array();
		foreach ($subjectDataStructure1 as $cummulativeSemester => $subjectList1)	{
			$dataArray1 = array();
			$currentYearArr = System::getYearAndSemesterFromACummulativeSemester($cummulativeSemester, $profile1);
			$currentYear = $currentYearArr['year'];
			if (! isset($yearDataArray1[$currentYear])) $yearDataArray1[$currentYear] = array();
			$currentSemester = $currentYearArr['semester'];
			$yearDataArray1[$currentYear][$currentSemester] = array();
			foreach ($subjectList1 as $index => $transactionId)	{
				$dataArray1['transactionId'] = $transactionId;
				$transaction1 = new CourseAndSubjectTransaction($database, $transactionId, $conn);
				$subject1 = $transaction1->getSubject();
				if (is_null($subject1)) Object::shootException("The Subject Could not be found or is empty");
				$dataArray1['subjectName'] = $subject1->getSubjectName();
				$dataArray1['subjectCode'] = $subject1->getSubjectCode();
				$dataArray1['totalUnits'] = $subject1->getTotalUnits();
				//Not Subject and Results share indices 
				if (isset($resultsDataStructure1[$cummulativeSemester]) && isset($resultsDataStructure1[$cummulativeSemester][$index]) && ($resultsDataStructure1[$cummulativeSemester][$index]['transactionId'] == $transactionId))	{
					//We are sure now we are dealing with the correct results 
					$resultsArray1 = $resultsDataStructure1[$cummulativeSemester][$index];
					$dataArray1['rawMarks'] = $resultsArray1['rawMarks'];
					$dataArray1['gradeId'] = $resultsArray1['gradeId'];
					$grade1 = new Grade($database, $dataArray1['gradeId'], $conn);
					$dataArray1['gradeName'] = $grade1->getGradeName();
					$dataArray1['gradePoint'] = $grade1->getGradePoint();
				}
			}
			$dataArray1['semester'] = $cummulativeSemester;
			$dataArray1['year'] = $currentYear;
			//dealing with semester and year gpa 
			$semesterBlock1 = $gpaDataStructure1[$cummulativeSemester];
			$headers['sem_'.$cummulativeSemester.'_gpa'] = $semesterBlock1['gpa'];
			$yearDataArray1[$currentYear][$currentSemester]['gpa'] = $semesterBlock1['gpa'];
			$headers['sem_'.$cummulativeSemester.'_awardId'] = $semesterBlock1['awardId'];
			$award1 = new ClassOfAward($database, $semesterBlock1['awardId'], $conn);
			$headers['sem_'.$cummulativeSemester.'_awardName'] = $award1->getAwardName();
			$dataLine1 = new TranscriptDataLine(TranscriptDataLine::$__IS_CLONE_MODE);
			$dataLine1->assignData($dataArray1);
			$dataLines[sizeof($dataLines)] = $dataLine1;
		}
		//Finalizing Data for year 
		foreach ($yearDataArray1 as $year => $semesterBlock1)	{
			$totalGPA = 0.0;
			$semesterCount = 0;
			foreach ($semesterBlock1 as $semester => $dataBlock1)	{
				$totalGPA = $totalGPA + floatval($dataBlock1['gpa']);
				$semesterCount++;
			}
			if ($semesterCount != 0)	{
				$totalGPA = ($totalGPA / $semesterCount);
				$totalGPA = Number::truncateFloatDecimal($totalGPA, Object::$__GPA_PRECISION);
				//Need to find its equivalent ClassOfAward 
				$awardId = ClassOfAward::getClassOfAwardFromGPAAndEducationLevel($database, $conn, $totalGPA, $level1->getLevelId());
				$totalGPA = Number::displayFloatDecimal($totalGPA, Object::$__GPA_PRECISION);
				//Saving 
				$headers['year_'.$year.'_gpa'] = $totalGPA;
				$headers['year_'.$year.'_awardId'] = $awardId;
				if (! is_null($awardId))	{
					$award1 = new ClassOfAward($database, $awardId, $conn);
					$headers['year_'.$year.'_awardName'] = $award1->getAwardName();
				}
			}
		}
		$filename = "student_".$student1->getStudentId().".csv";
		if (! is_null($student1->getTranscriptFile()))	$filename = $student1->getTranscriptFile();
		$headers['filename'] = $filename;
		$filename = $transcriptFolder.$filename;
		$transcriptFile1->assignData($database, $conn, $filename, $headers, $dataLines);
		return $transcriptFile1;
	}
	public function save()	{
		$promise1 = new Promise();
		$promise1->setPromise(true);
		if (file_exists($this->filename)) $promise1 = FileFactory::createBackupFile($this->filename);
		if (! $promise1->isPromising()) Object::shootException("Could not create a backup file");
		$lineCount = 0;
		$file1 = fopen($this->filename, "w") or Object::shootException("Could not open file for writing");
		//writing Headers 
		foreach ($this->headers as $key => $val)	{
			$lineCount++;
			$line = "@-header,".$key."_!_".$val;
			for ($i = 2; $i < Object::$numberOfTranscriptFileColumns; $i++)	{
				$line .= ",";
			}
			$line .= "\n";
			fwrite($file1, $line) or Object::shootException("Could not write to a file [$lineCount]");
		}
		//Begin Instruction 
		$lineCount++;
		$line = "@-business-logic-begin";
		for ($i = 1; $i < Object::$numberOfTranscriptFileColumns; $i++)	{
			$line .= ",";
		}
		$line .= "\n";
		fwrite($file1, $line) or Object::shootException("Could not write to a file [$lineCount]");
		foreach ($this->dataLines as $dataLine1)	{
			$lineCount++;
			if (is_null($dataLine1)) continue;
			$dataLine1->synchronize();
			$line = $dataLine1->getLine(); 
			$line = str_replace("\n","", str_replace("\r", "", $line));
			$line .= "\n";
			fwrite($file1, $line) or Object::shootException("Could not write to a file [$lineCount]");
		}
		//End Instruction 
		$lineCount++;
		$line = "@-business-logic-end";
		for ($i = 1; $i < Object::$numberOfTranscriptFileColumns; $i++)	{
			$line .= ",";
		}
		$line .= "\n";
		fwrite($file1, $line) or Object::shootException("Could not write to a file [$lineCount]");
		fclose($file1);
		//Entering Saving Mode 
		$checksum = md5_file($this->filename);
		$student1 = new Student($this->database, $this->headers['studentId'], $this->conn);
		$student1->setTranscriptFile($this->headers['filename']);
		$student1->setTranscriptHash($checksum);
		$student1->commitUpdate();
		return $promise1;
	}
	public function cloneMe()	{
		$data1 = new TranscriptFile("Ndimangwa", TranscriptFile::$__IS_CLONE_MODE, "Fadhili");
		$headers = array();
		foreach ($this->headers as $key => $val)	{
			$headers[$key] = $val;
		}
		$dataLines = array();
		foreach ($this->dataLines as $key => $dataLine1)	{
			$dataLines[$key] = $dataLine1->cloneMe();
		}
		$data1->assignData($this->database, $this->conn, $this->filename, $headers, $dataLines);
		return $data1;
	}
	public function assignData($database, $conn, $filename, $headers, $dataLines)	{
		$this->database = $database;
		$this->conn = $conn;
		$this->filename = $filename;
		$this->headers = $headers;
		$this->dataLines = $dataLines;
	}
	public function getCompilationTime()	{
		$compilationTime1 = null;
		try {
			$compilationTime1 = new DateAndTime("Ndimangwa", $this->headers['compilationTime'], "Fadhili");
		} catch (Exception $e)	{
			$compilationTime1 = null;
		}
		return $compilationTime1;
	}
	public function getStudentId()	{ return $this->headers['studentId']; }
	public function getStudent()	{
		$student1 = null;
		try	{
			$student1 = new Student($this->database, $this->headers['studentId'], $this->conn);
		} catch (Exception $e)	{
			$student1 = null;
		}
		return $student1;
	}
	public function getCourseId()	{ return $this->headers['courseId']; }
	public function getCourseName()	{ return $this->headers['courseName']; }
	public function getDuration()	{ return intval($this->headers['duration']); }
	public function getCourse()	{
		$course1 = null;
		try {
			$course1 = new Course($this->database, $this->headers['courseId'], $this->conn);
		} catch (Exception $e)	{
			$course1 = null;
		}
		return $course1;
	}
	public function getGPA()	{ return $this->headers['gpa']; }
	public function isCleared()	{ return (intval($this->headers['cleared']) == 1); }
	public function isDisqualified()	{ return (intval($this->headers['disqualified']) == 1); }
	public function getAwardId()	{ return $this->headers['awardId']; }
	public function getAwardName()	{ return $this->headers['awardName']; }
	public function getAward()	{
		$award1 = null;
		try {
			$award1 = new ClassOfAward($this->database, $this->headers['awardId'], $this->conn);
		} catch (Exception $e)	{
			$award1 = null;
		}
		return $award1;
	}
	public function getStudentFilename()	{ return $this->headers['filename']; }
}
class TranscriptDataLine	{
	private $line;
	private $transactionId;
	private $semester;
	private $year;
	private $subjectName;
	private $subjectCode;
	private $rawMarks;
	private $gradeId;
	private $gradeName;
	private $gradePoint;
	private $totalUnits;
	static $__IS_CLONE_MODE = "is_clone_mode";
	public function __construct($line)	{
		if ($line == TranscriptDataLine::$__IS_CLONE_MODE) return;
		$this->line = $line;
		$lineArr = explode(",", $line);
		if (sizeof($lineArr) != Object::$numberOfTranscriptFileColumns) return;
		$this->transactionId = $lineArr[0];
		$this->semester = $lineArr[1];
		$this->year = $lineArr[2];
		$this->subjectName = $lineArr[5];
		$this->subjectCode = $lineArr[6];
		$this->rawMarks = $lineArr[7];
		$this->gradeId = $lineArr[8];
		$this->gradeName = $lineArr[9];
		$this->gradePoint = $lineArr[10];
		$this->totalUnits = $lineArr[11];
	} 
	public function assignData($dataArray)	{
		if (isset($dataArray['transactionId'])) $this->transactionId = $dataArray['transactionId'];
		if (isset($dataArray['semester'])) $this->semester = $dataArray['semester'];
		if (isset($dataArray['year'])) $this->year = $dataArray['year'];
		if (isset($dataArray['subjectName'])) $this->subjectName = $dataArray['subjectName'];
		if (isset($dataArray['subjectCode'])) $this->subjectCode = $dataArray['subjectCode'];
		if (isset($dataArray['rawMarks'])) $this->rawMarks = $dataArray['rawMarks'];
		if (isset($dataArray['gradeId'])) $this->gradeId = $dataArray['gradeId'];
		if (isset($dataArray['gradeName'])) $this->gradeName = $dataArray['gradeName'];
		if (isset($dataArray['gradePoint'])) $this->gradePoint = $dataArray['gradePoint'];
		if (isset($dataArray['totalUnits'])) $this->totalUnits = $dataArray['totalUnits'];
		if (isset($dataArray['line'])) $this->line = $dataArray['line'];
	}
	public function cloneMe()	{
		$data1 = new TranscriptDataLine(TranscriptDataLine::$__IS_CLONE_MODE);
		$dataArray = array();
		$dataArray['transactionId'] = $this->transactionId;
		$dataArray['semester'] = $this->semester;
		$dataArray['year'] = $this->year;
		$dataArray['subjectName'] = $this->subjectName;
		$dataArray['subjectCode'] = $this->subjectCode;
		$dataArray['rawMarks'] = $this->rawMarks;
		$dataArray['gradeId'] = $this->gradeId;
		$dataArray['gradeName'] = $this->gradeName;
		$dataArray['gradePoint'] = $this->gradePoint;
		$dataArray['totalUnits'] = $this->totalUnits;
		$dataArray['line'] = $this->line;
		$data1->assignData($dataArray);
		return $data1;
	}
	public function synchronize()	{
		//Should be called prior to saving 
		$line = $this->transactionId;
		$line .= ",".$this->semester;
		$line .= ",".$this->year;
		$line .= ",,";
		$line .= ",".$this->subjectName;
		$line .= ",".$this->subjectCode;
		$line .= ",".$this->rawMarks;
		$line .= ",".$this->gradeId;
		$line .= ",".$this->gradeName;
		$line .= ",".$this->gradePoint;
		$line .= ",".$this->totalUnits;		
		$this->line = $line;
	}
	public function setTransactionId($transactionId)	{ $this->transactionId = $transactionId; }
	public function getTransactionId()	{ return $this->transactionId; }
	public function setCummulativeSemester($semester) { $this->semester = $semester; }
	public function getCummulativeSemester()	{ return $this->semester; }
	public function setYear($year) { $this->year = $year; }
	public function getYear()	{ return $this->year; }
	public function setSubjectName($subjectName)	{ $this->subjectName = $subjectName; }
	public function getSubjectName()	{ return $this->subjectName; }
	public function setSubjectCode($subjectCode)	{ $this->subjectCode = $subjectCode; }
	public function getSubjectCode()	{ return $this->subjectCode; }
	public function setRawMarks($rawMarks)	{ $this->rawMarks = $rawMarks; }
	public function getRawMarks()	{ return $this->rawMarks; }
	public function setGradeId($gradeId)	{ $this->gradeId = $gradeId; }
	public function getGradeId()	{ return $this->gradeId; }
	public function setGradeName($gradeName) { $this->gradeName = $gradeName; }
	public function getGradeName()	{ return $this->gradeName; }
	public function setGradePoint($gradePoint)	{ $this->gradePoint = $gradePoint; }
	public function getGradePoint()	{ return $this->gradePoint; }
	public function setTotalUnits($totalUnits)	{ $this->totalUnits = $totalUnits; }
	public function getTotalUnits()	{ return $this->totalUnits; }
	public function getLine()	{ return $this->line; }
}
?>