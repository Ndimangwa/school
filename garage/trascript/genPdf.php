<?php 
require_once("../../class/system.php");
require '../../vendor/autoload.php';
use mikehaertl\wkhtmlto\Pdf;
$config = "../../config.php";
include($config);
$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
$profile1 = new Profile($database, Profile::getProfileReference($database, $conn), $conn);
//$transcript1 = Student::createTranscript($database, $conn, $profile1, $systemTime1, $studentId, $transcriptFolder, $resultsFolder, $upToYear, $upToSemester, $groupList, $rawMarksResultsFileArrayData1, $gradedMarksResultsFileArrayData1, $controlFlags);
$studentId = 773; //Joseph Elimringi Mshiu ; BHLS Year 3 in 2016/2017
$dataFolder = "../../data/";
$upToYear = 3;
$upToSemester = 2;
$count = 0;
try {
	$transcriptFolder = $dataFolder."transcripts/";
	$filename = $transcriptFolder."transcript_".$studentId.".csv";
	$transcript1 = new TranscriptFile($database, $filename, $conn);
	$transcript1->putData('profile', $profile1);
	$transcript1->putData('dataFolder', $dataFolder);
	$transcript1->putData('pdfEngine', new Pdf());
	$transcript1->savePDF();
} catch (Exception $e)	{
	$message = $e->getMessage();
	mysql_close($conn);
	die("Failed with $message");
}
mysql_close($conn);
echo "\nCOMPLETED\n";
?>