if ($page == "managebriefcasegroup_edit" && isset($_REQUEST['id']) && isset($_REQUEST['report']) && isset($_REQUEST['detach_login']) && Authorize::isAllowable($config, "managebriefcasegroup_edit", "normal", "setlog", "-1", "-1"))	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$briefcaseGroup1 = null;
		$headerText = "UNKNOWN";
		try {
			$briefcaseGroup1 = new BriefcaseGroup($database, $_REQUEST['id'], $conn);
			$headerText = "De-Assign Event Creators Map from ".$briefcaseGroup1->getGroupName();
			if (! isset($_REQUEST['did'])) Object::shootException("There were nothing to Attach");
			$listToUpdate = Object::detachIdListFromObjectList($briefcaseGroup1->getListOfOwners(), $_REQUEST['did']);
			//Note The Primary Owner can not be Detached 
			$collection1 = new Collection();
			$collection1->addCommaSeparatedList($listToUpdate);
			$primaryOwner1 = $briefcaseGroup1->getLogin();
			if (is_null($primaryOwner1)) Object::shootException("Primary Owner were not found");
			if (! $collection1->isItemInACollection($primaryOwner1->getLoginId())) Object::shootException("The Primary Owner Can not be removed");
			$briefcaseGroup1->setListOfOwners($listToUpdate);
			$briefcaseGroup1->commitUpdate();
			$enableUpdate = true;
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
		<div class="ui-sys-panel-container ui-sys-panel ui-widget ui-widget-content">
			<div class="ui-sys-panel-header ui-widget-header"></div>
			<div class="ui-sys-panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Owner List Attachment were removed successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in De-Attaching Owner List <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>			
			</div>
			<div class="ui-sys-panel-footer"><a class="ui-sys-back" href="<?= $thispage ?>?page=managebriefcasegroup">Back to BriefcaseGroup Manager</a></div>
		</div>
<?php
		mysql_close($conn);
		if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managebriefcasegroup_edit", "Detach Owners for ".$briefcaseGroup1->getGroupName());
	} else if ($page == "managebriefcasegroup_edit" && isset($_REQUEST['id']) && isset($_REQUEST['report']) && isset($_REQUEST['attach_login']) && Authorize::isAllowable($config, "managebriefcasegroup_edit", "normal", "setlog", "-1", "-1"))	{
		$conn = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$enableUpdate = false;
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$briefcaseGroup1 = null;
		$headerText = "UNKNOWN";
		try {
			$briefcaseGroup1 = new BriefcaseGroup($database, $_REQUEST['id'], $conn);
			$headerText = "Assign Owner Map to ".$briefcaseGroup1->getGroupName();
			if (! isset($_REQUEST['did'])) Object::shootException("There were nothing to Attach");
			$listToUpdate = Object::attachIdListToObjectList($briefcaseGroup1->getListOfOwners(), $_REQUEST['did']);
			$briefcaseGroup1->setListOfOwners($listToUpdate);
			$briefcaseGroup1->commitUpdate();
			$enableUpdate = true;
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
		<div class="ui-sys-panel-container ui-sys-panel ui-widget ui-widget-content">
			<div class="ui-sys-panel-header ui-widget-header"></div>
			<div class="ui-sys-panel-body">
<?php 
	if ($promise1->isPromising())	{
?>
		<div class="ui-state-highlight">
			Owner List Attachment were done successful
		</div>
<?php
	} else {
?>
		<div class="ui-state-error">
			There were problems in Attaching Owner List <br/>
			Details: <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>			
			</div>
			<div class="ui-sys-panel-footer"><a class="ui-sys-back" href="<?= $thispage ?>?page=managebriefcasegroup">Back to BriefcaseGroup Manager</a></div>
		</div>
<?php
		mysql_close($conn);
		if ($enableUpdate) Accounting::addLog($config, $date, $login1->getLoginName(), "managebriefcasegroup_edit", "Attach Owners for ".$briefcaseGroup1->getGroupName());
	} else if ($page == "managebriefcasegroup_edit" && isset($_REQUEST['id']) && isset($_REQUEST['attach_login']) && Authorize::isAllowable($config, "managebriefcasegroup_edit", "normal", "setlog", "-1", "-1")) {
		$conn  = mysql_connect($hostname, $user, $pass) or die("Could not connect to a database services");
		$promise1 = new Promise();
		$promise1->setPromise(true);
		$briefcaseGroup1 = null;
		$headerText = "UNKNOWN";
		try {
			$briefcaseGroup1 = new BriefcaseGroup($database, $_REQUEST['id'], $conn);
			$headerText = "Assign Owner Map to ".$briefcaseGroup1->getGroupName();
		} catch (Exception $e)	{
			$promise1->setReason($e->getMessage());
			$promise1->setPromise(false);
		}
?>
		<div class="ui-sys-panel-container ui-sys-panel ui-widget ui-widget-content">
			<div class="ui-sys-panel-header ui-widget-header"><?= $headerText ?></div>
			<div class="ui-sys-panel-body ui-sys-search-results">
<?php 
	if ($promise1->isPromising())	{
/*Begin of BLOCK ONE */
?>
		<!--Begin of Cross Reference List Insertion-->
<?php 
	if (! is_null($briefcaseGroup1->getListOfOwners()) && sizeof($briefcaseGroup1->getListOfOwners()) > 0)	{
		$objectList1 = $briefcaseGroup1->getListOfOwners();
?>
			<form method="POST" action="<?= $thispage ?>">
				<input type="hidden" name="page" value="managebriefcasegroup_edit"/>
				<input type="hidden" name="id" value="<?= $briefcaseGroup1->getGroupId() ?>"/>
				<input type="hidden" name="report" value="true"/>
				<input type="hidden" name="detach_login" value="true"/>
				<table class="pure-table pure-table-aligned ui-sys-table-search-results ui-sys-table-search-results-1">
					<thead>
						<tr>
						<th colspan="6">BriefcaseGroupd By</th>
						</tr>
						<tr>
							<th></th>
							<th></th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>Status</th>
						</tr>
					</thead>
<?php 
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	foreach ($objectList1 as $obj1)	{
		if (is_null($obj1)) continue;
		if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
			$numberOfTBodies++;
			$tbodyClosed = false;
		} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
			echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
			$numberOfTBodies++;
			$perTbodyCounter = 0;
		}
		//In All Cases we need to display tr
		$pureTableOddClass = "";
		if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
		<tr class="<?= $pureTableOddClass ?>">
			<td><input type="checkbox" name="did[<?= $sqlReturnedRowCount ?>]" value="<?= $obj1->getLoginId() ?>"/></td>
			<td><?= $sqlReturnedRowCount + 1 ?></td>
			<td><?= $obj1->getFullname() ?></td>
			<td><?= $obj1->getEmail() ?></td>
			<td><?= $obj1->getPhone() ?></td>
			<td>
<?php 
	if (! is_null($obj1->getUserStatus()))	{
		echo $obj1->getUserStatus()->getStatusName();
	}
?>			
			</td>
		</tr>
<?php
		$perTbodyCounter++;
		$sqlReturnedRowCount++;
	}
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
				</table>
				<div class="ui-sys-right">
					<input type="submit" value="Detach Owners"/>
				</div>
			</form>
			<ul class="ui-sys-pagination-1"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination-1').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results-1 tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>

<?php
	}
?>
			<!--End of Cross Reference List Insertion-->
<?php
/*End of BLOCK ONE*/
/*Begin of BLOCK TWO -- Extra*/
?>
	<div style="padding: 1px; border: 1px gold dotted;">
		<!--Content of Block Two Extra BEgins-->
		<!--Block ONE controls for searching-->
				<div class="ui-sys-search-container pure-form pure-transaction-control">
					<input type="text" title="At Least Three Characters should be supplied" required placeholder="ABC" size="32"/>
					<a title="Click to Search" class="click-to-search" data-next-page="<?= $thispage ?>?page=managebriefcasegroup_edit&id=<?= $briefcaseGroup1->getGroupId() ?>&attach_login=true" data-min-character="3"><img src="../sysimage/buttonsearch.png" alt="DAT"/></a>
				</div>
<!--Block ONE ends-->	
<?php 
	if (isset($_REQUEST['searchtext']))	{
		$searchtext = $_REQUEST['searchtext'];
		//saving Authorization Rules 
?>
<!--Block Two, Search Results-->
<div class="ui-sys-search-results">
	<label id="statustextlabel"></label>
	<form method="POST" action="<?= $thispage ?>">
		<input type="hidden" name="page" value="managebriefcasegroup_edit"/>
		<input type="hidden" name="id" value="<?= $briefcaseGroup1->getGroupId() ?>"/>
		<input type="hidden" name="report" value="true"/>
		<input type="hidden" name="attach_login" value="true"/>
		<table class="pure-table ui-sys-table-search-results-2">
			<thead>
				<tr>
					<th></th>
					<th>S/N</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Status</th>
				</tr>
			</thead>
<?php 
	$query = Login::getQueryText();
	$sqlReturnedRowCount = 0;
	$perTbodyCounter = 0;
	$numberOfTBodies = 0;
	$result = mysql_db_query($database, $query, $conn) or die("Could not fetch transaction data from the storage");
	$maximumReturnedRows = intval($profile1->getMaximumNumberOfReturnedSearchRecords());
	$maximumRowsPerPage = intval($profile1->getMaximumNumberOfDisplayedRowsPerPage());
	$tbodyClosed = true;
	while (list($id) = mysql_fetch_row($result))	{
		if ($sqlReturnedRowCount > $maximumReturnedRows)	{
			//Finalize All House Keeping Here
			break;
		}
		$transaction1 = null;
		try {
			$transaction1 = new Login($database, $id, $conn);
		} catch (Exception $e)	{ die($e->getMessage()); }
		$matrix1 = new SearchMatrix($searchtext); //Need to be recreated per objects 
		if ($transaction1->searchMatrix($matrix1)->evaluateResult())	{
			if ($tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "<tbody tbody-index=\"$numberOfTBodies\">"; 
				$numberOfTBodies++;
				$tbodyClosed = false;
			} else if (! $tbodyClosed && ($sqlReturnedRowCount % $maximumRowsPerPage) == 0)	{
				echo "</tbody><tbody tbody-index=\"$numberOfTBodies\" class=\"ui-sys-hidden\">";
				$numberOfTBodies++;
				$perTbodyCounter = 0;
			}
			//In All Cases we need to display tr
			$pureTableOddClass = "";
			if (($perTbodyCounter % 2) != 0) $pureTableOddClass = "pure-table-odd";
?>
			<tr class="<?= $pureTableOddClass ?>">
				<td><input type="checkbox" name="did[<?= $sqlReturnedRowCount ?>]" value="<?= $transaction1->getLoginId() ?>"/></td>
				<td><?= $sqlReturnedRowCount + 1 ?></td>
				<td><?= $transaction1->getFullname() ?></td>
				<td><?= $transaction1->getEmail() ?></td>
				<td><?= $transaction1->getPhone() ?></td>
				<td>
<?php 
	if (! is_null($transaction1->getUserStatus()))	{
		echo $transaction1->getUserStatus()->getStatusName();
	}
?>				
				</td>
			</tr>
<?php
			$perTbodyCounter++;
			$sqlReturnedRowCount++;
		}//end-evaluate-results
	}//end-while
	if (! $tbodyClosed)	{
		echo "</tbody>";
 		$tbodyClosed = true;
	}
?>
		</table>
		<div class="ui-sys-right">
			<input type="submit" value="Attach Owners"/>
		</div>
	</form>
	<input type="hidden" id="saveresultsstorage" value="<?= $sqlReturnedRowCount ?>"/>
	<ul class="ui-sys-pagination-2"></ul>
<script type="text/javascript">
(function($)	{
	$(function()	{
		 $('ul.ui-sys-pagination-2').twbsPagination({
			totalPages: <?= $numberOfTBodies ?>,
			visiblePages: 5,
			onPageClick: function (event, page) {
				//page is page number 
				var $tbodyList1 = $('table.ui-sys-table-search-results-2 tbody');
				if (! $tbodyList1.length) return;
				//Hide All tbody 
				$tbodyList1.addClass('ui-sys-hidden');
				//Now show only the one corresponding to this page
				$tbodyList1.eq(page-1).removeClass('ui-sys-hidden');			
			}
		});
	});
})(jQuery);
</script>
</div>
<!--Block Two Ends-->
<?php
	} //end-if-searchtext
?>		
		<!--Content of Block Two Extra Ends-->
	</div>
<?php
/*End of BLOCK TWO -- Extra*/
	} else {
?>
		<div class="ui-state-error">
			There were problems in the initial setup of Assignment <br/>
			Reason : <?= $promise1->getReason() ?>
		</div>
<?php
	}
?>			
			</div>
			<div class="ui-sys-panel-footer"><a class="ui-sys-back" href="<?= $thispage ?>?page=managebriefcasegroup">Back to BriefcaseGroup Manager</a></div>
		</div>
<?php
		mysql_close($conn);
	} else 