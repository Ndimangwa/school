<html>
<head>
<title>My Web Test</title>
<link rel="stylesheet" type="text/css" media="all" href="../../client/plugin/chromose/chromoselector-2.1.8/chromoselector.css"/>
<style type="text/css">

div.container	{
	width: 80%; margin: 1px; margin-left: auto; margin-right: auto; padding: 2px;
}

</style>
<script type="text/javascript" src="../../client/jquery.js"></script>
<script type="text/javascript" src="../../client/plugin/chromoselector-2.1.8/chromoselector.min.js"></script>
<script type="text/javascript">

(function($)	{
	$(function()	{
		$('#color').chromoselector();
	});
})(jQuery);

</script>
</head>
<body>
	<div class="container">
<!--BEGIN CONTENT-->
<?php
	if (isset($_REQUEST['color']))	{
		$color = $_REQUEST['color'];
		echo "Submitted Color is $color"; 
		echo "<br/>";
		$backlink = $_SERVER['PHP_SELF'];
		echo "<a href=\"$backlink\">Click Here</a> to go back";
	} else {
		//Default Landing Page
?>
		<form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
			<input type="hidden" name="submit" value="10"/>
			<input type="text" id="color" name="color" size="6" />
			<input type="submit" value="Submit"/>
		</form>
<?php
	}
?>
<!--END CONTENT-->	
	</div>
</body>
</html>